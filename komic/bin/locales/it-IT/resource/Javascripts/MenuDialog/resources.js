// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

var KomicResources = (function() {
	var strings = {		
			version: {
				ver: 'Komic v2.3',
				shortversion: 'v2.3'
			},	
			menu: {
				title: 'Menu',
				orientationLandscape: 'Passa alla modalit� orizzontale',
				orientationPortrait: 'Passa alla modalit� verticale',
				lastread: ''
			},
			bookmarkmenu: {
				title: 'Menu Segnalibri',
				addtobookmark: 'Aggiungi segnalibro',
				jumptobookmark: 'Salta ad un segnalibro',
				removebookmark: 'Rimuovi segnalibro'
			},	
			readingmenu: {
				title: 'Menu Lettura',
				returnhome: 'Ritorna alla HOME',
				choosecomic: 'Scegli un fumetto',
				jumptopage: 'Salta alla pagina',
				changesize: 'Cambia la dimensione della pagina',
				lock: 'Blocca la dimensione della pagina',
				unlock: 'Sblocca la dimensione della pagina',
				reset: 'Ripristina la dimensione della pagina',
				screenshot: 'Fai uno screenshot',
				addtodelete: 'Pianifica la rimozione',
				removefromdelete: 'Annulla la rimozione',
				scroll: 'Modifica il valore dello scroll',
				autonext: "Attiva l'avanzamento automatico",
				contrast: 'Cambia contrasto e luminosit�',
				resetcontrastbrigthness: 'Ripristina contrasto e luminosit�'
			},
			colormenu: {
				title: 'Colore di sfondo',
				white: 'Bianco',
				gray: 'Grigio',
				black: 'Nero',
				custom: 'Scegli il colore',
				currentcolor: 'Colore corrente: ',
				custommessage: 'Immetti il colore',
				colortitle: 'Scelta del colore di sfondo',
				choosecolor: 'Per favore scegli un colore'
			},			
			refreshmenu: {
				title: 'Frequenza aggiornamento',
				fivepages: 'Ogni 5 pagine',
				tenpages: 'Ogni 10 pagine',
				twentypages: 'Ogni 20 pagine',
				never: 'Mai',
				custom: 'Scegli ogni quante pagine',
				currentrate: 'Impostazione corrente: ',
				custommessage: 'Scegli ogni quante pagine aggiornare lo schermo',
				refreshtitle: 'Scelta della frequenza',
				chooserate: 'Per favore scegli un valore'
			},			
			timermenu: {
				title: 'Timer avanzamento automatico',
				message: "Scegli il timer per l'avanzamento",
				info: 'Esprimi il timer in secondi',
				choosetimer: 'Per favore scegli un tempo'
			},			
			optionsHTML: {
				autoSaveOption: 'Autosave',
				autoJumpOption: 'Autojump',
				zoomButtonsOption: 'Pulsanti di zoom',
				landscapeOrientationOption: 'Senso di rotazione',
				keepImageRatioOption: 'Mantieni proporzioni',
				rememberOrientationOption: 'Ricorda orientazione',
				jumpToLastReadOption: "Vai all'ultimo fumetto",
				coverViewOption: 'Cover view',
				soundsOption: 'Suoni',
				saveButtonOption: 'Salvataggio veloce',
				titleBarOption: 'Barra di stato',
				debugMessagesOption: 'Messaggi debug',
				preloadOption: 'Preload immagini',
				backgroundColorOption: 'Colore di sfondo',
				refreshRateOption: 'Refresh schermo',
				calibrationOption: 'Calibrazione tocco',
				calibrationOptionText0: "Tocca l'area",
				calibrationOptionText1: "Tocca ancora",
				calibrationOptionText2: "Tocca l'ultima volta",
				calibrationOptionText3: "Calibrazione finita!",
				calibrationOptionAverage: "Valori medi"
			},			
			readingHTML: {
				scroll: 'Scegli il valore per lo scroll',
				value: 'Valore attuale:',
				contrast: 'Contrasto',
				brightness: 'Luminosit�',
				reset: 'Resetta',
				confirm: 'Conferma'
			},
			infopageHTML: {
				title: 'Titolo',
				series: 'Serie',
				number: 'Numero',
				notes: 'Note',
				year: 'Anno',
				month: 'Mese',
				writer: 'Scrittore',
				penciller: 'Disegnatore',
				colorist: 'Colorista',
				publisher: 'Editore',
				genre: 'Genere',
				web: 'Link web',
				pagecount: 'Numero pagine',
				languageISO: 'Lingua',
				communityrating: 'Punteggio della comunit�'
			},				
			deleteJS: {
				available: 'Rimozioni pianificate:',
				noavailable: 'Nessuna rimozione pianificata!',
				removed: 'Rimozione effettuata con successo! Attendi la creazione del nuovo indice.',
				scheduled: 'Gi� pianificato!',
				popuptitleadd: 'I tuoi fumetti',
				popuptitleremove: 'Le tue rimozioni pianificate',
				error: 'Errore: nessuna rimozione pianificata o nessun fumetto trovato!',
				working: 'Rimozione in corso, attendi...'
			},		
			bordersJS: {
				available: 'Fumetto da processare:',
				noavailable: 'Nessun fumetto da processare!',
				removed: 'Bordi rimossi con successo!',
				scheduled: 'Gi� pianificato!',
				popuptitleadd: 'I tuoi fumetti',
				previewdone: "Ora vedrai un'anteprima del risultato.<br>Premi sull'immagine per uscire",
				previewerror: "C'� stato un errore durante il processo",
				choosetolerance: 'Scegli la tolleranza nella rimozione dei bordi',
				tolerancetitle: 'Scelta della tolleranza del processo',
				continueprocess: 'Continuare con il processo?',
				error: 'Errore: nessun fumetto trovato o errore durante la rimozione dei bordi!',
				working: 'Rimozione dei bordi in corso, attendi...'
			},			
			indexJS: {
				coverview: "La cover view � disabilitata.<br>Abilita l'opzione nel menu opzioni e riavvia Komic",
				firstRun: 'Questa � la tua prima volta in Komic!<br>Visita la pagina di Help e/o il Changelog.<br>Divertiti!',
				updated: 'Hai appena aggiornato Komic.<br>Guarda il Changelog e, se necessario, visita la pagina di Help.<br>Divertiti!',
				saving: 'Sto salvando, attendere...'
			},			
			optionJS: {
				optionsTitle0: 'Menu opzioni 1/4',
				optionsTitle1: 'Menu opzioni 2/4',
				optionsTitle2: 'Menu opzioni 3/4',
				optionsTitle3: 'Menu opzioni 4/4',
				autoSaveOption: 'Salva automaticamente un segnalibro quando si torna alla HOME di Komic o si cambia fumetto',
				autoJumpOption: 'Salta automaticamente al fumetto successivo/precedente quando si arriva alla fine/inizio di un fumetto',
				zoomButtonsOption: 'Abilita o disabilita i pulsanti di zoom',
				landscapeOrientationOption: 'Imposta la rotazione oraria o antioraria per la modalit� orizzontale',
				keepImageRatioOption: "Mantiene le proporzioni delle immagini o allarga l'immagine a schermo intero",
				rememberOrientationOption: "Sceglie se ricordare o meno l'ultima orientazione usata",
				jumpToLastReadOption: "Se attiva, sarai portato automaticamente all'ultimo fumetto letto quando avvii Komic",
				coverViewOption: "Abilita o disabilita la cover view.<br>Una volta in cover view, tocca l'intestazione per tornare alla HOME di Komic.<br>Tocca una copertina per ingrandirla quindi tocca ancora la copertina per aprire il fumetto o tocca sullo sfondo attorno per tornare alla cover view",
				soundsOption: 'Permette di abilitare o disabilitare i suoni',
				saveButtonOption: "Abilita o disabilita l'area touch nell'angolo in alto a destra per salvare velocemente un segnalibro",
				titleBarOption: 'Abilita o disabilita la barra di stato nella parte alta dello schermo',
				debugMessagesOption: 'Abilita o disabilita i messaggi di debug degli script',
				preloadOption: 'Abilita o disabilita il precaricamento delle immagini durante la lettura',
				refreshRateOption: 'Permette di scegliere ogni quanto aggiornare completamente lo schermo del Kindle',
				backgroundColorOption: 'Permette di scegliere il colore dello sfondo durante la lettura',				
				calibrationOption: 'Permette di calibrare i dati riguardanti il tocco',
				soundsON: 'Suoni abilitati',
				soundsOFF: 'Suoni disabilitati'
			},			
			readingJS: {
				nomorecomics: 'Nessun altro fumetto disponibile!',
				lastpage: "Sei gi� all'ultima pagina!",
				firstpage: 'Sei gi� alla prima pagina!',
				firstcomic: 'Sei gi� al primo fumetto!',
				bmrksaved: 'Segnalibro salvato!',
				choosepage: 'Per favore scegli una pagina',
				nonexistingpage: 'Hai scelto una pagina non disponibile!',
				nobmrkfolder: 'Non ci sono segnalibri per questo fumetto!',
				bmrkremoved: 'Segnalibro rimosso!',
				nobmrkpage: "Non c'� un segnalibro in questa pagina!",
				alreadybmrk: 'Pagina gi� salvata!',
				savebmrkprompttitle: "Descrizione segnalibro",
				savebmrkpromptmessage: "Scegli la descrizione per il segnalibro",
				nodescription: "Nessuna descrizione",
				choosesize: 'Per favore scegli una dimensione',
				locked: "Dimensione dell'immagine bloccata",
				unlocked: "Dimensione dell'immagine sbloccata",
				screenshot: "Screenshot fatto!",
				deletionscheduled: 'Rimozione pianificata!',
				deletionremoved: 'Pianificazione della rimozione annullata!',
				jumpprompttitle: 'Salta alla pagina',
				jumppromptmessage: 'Inserisci un numero di pagina:',
				sizeprompttitle: "Ridimensiona l'immagine",
				sizepromptmessage: "Inserisci la nuova larghezza in %<br>(non aggiungere il simbolo %):",
				sizeconfirmmessage: "Cambiare anche l'altezza?",
				titlebardeletion: '--- RIMOZIONE DEL FUMETTO PIANIFICATA ---',
				jumpfirst: 'Prima pag.',
				jumplast: 'Ultima pag.',
				currentpage: 'Pagina attuale: '
			},	
			bookmarksJS: {
				available: 'Segnalibri disponibili:',
				popuptitle: 'I tuoi segnalibri'
			},
			jsonJS: {
				jsonreset: "Ops, non riesco a trovare le tue impostazioni. Le resetter� con i valori di default. Vai nella pagina delle opzioni per settarle nuovamente!"
			},
			infopageJS: {
				nofile: 'Nessun file ComicInfo.xml trovato per questo fumetto! Per favore scegli un altro fumetto.'
			},		
			shared: {
				managebookmarks: 'Gestisci i segnalibri',
				optionsmenu: 'Menu opzioni',
				infopage: 'Informazioni sui fumetti',
				removeborders: 'Rimuovi i bordi ai fumetti',
				coverview: 'Cover view',
				page: 'Pagina: ',
				chooseacomic: 'Scegli un fumetto',
				scheduledeletions: 'Pianifica le rimozioni',
				popupclose: 'Chiudi',
				nobookmarksavailable: 'Nessun segnalibro disponibile!',
				screenshots: 'Sposta gli screenshots',
				screenshotsMoving: 'Spostamento in corso, attendi...',
				screenshotsDone: 'Screenshots spostati e rinominati!',
				screenshotsError: 'Screenshots non trovati!',
				decompress: 'Decomprimi i file CBZ e CBR',
				decompressDone: 'CBZs or CBRs decompressi. Attendi la creazione del nuovo indice.',
				decompressError: "Non � stato trovato nessun file da decomprimere o c'� stato un errore durante la decompressione!",
				decompressing: 'Decompressione in corso, attendi...',
				helppage: 'Aiuto in linea',
				searchcomicmessage: 'Digita qualcosa da cercare',
				searchcomictitle: 'Ricerca fumetti'
			},
			buttons: {
				choosecomic: 'Scegli un Fumetto',
				coverview: 'Cover View',
				exit: 'Esci',
				menu: 'Menu',
				removeabookmark: 'Rimuovi un Segnalibro',
				removeall: 'Rimuovi Tutti',
				returnhome: 'Home',
				addto: 'Aggiungi',
				removefrom: 'Rimuovi',
				run: 'Avvia',
				bookmarksmenu: 'Menu Segnalibri',
				readingmenu: 'Menu Lettura',
				collapse: 'Collassa',
				search: 'Cerca...',
				restore: 'Ripristina'
			}	
	}; 
    return {
        strings: strings
    };
} ());