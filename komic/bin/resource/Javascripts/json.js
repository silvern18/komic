// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function resetOptionsJSON() {
	try {
		var optionsJSON = {'komicData': {
								'liveData' : {
									'lastread' : 'nothing',
									'nextFolder' : '1',
									'orientation' : 'portrait',
									'pagenum' : '0'
								},
								'options' : {
									'autojump' : 'OFF',
									'autosave' : 'OFF',
									'backgroundcolor' : 'white',
									'brightnesscontrast' : '0/0',
									'calibration': '10/100',
									'debugmessage' : 'OFF',
									'imageratio' : 'OFF',
									'komicsave' : 'OFF',
									'komicsounds' : 'ON',
									'komictitlebar' : 'ON',
									'landscape' : 'landscapeRight',
									'preview' : 'OFF',
									'prealodImages' : 'OFF',
									'refreshrate' : 'never',
									'rememberLastRead' : 'OFF',
									'rememberMode' : 'OFF',
									'scrollheight' : '277',
									'zoom' : 'OFF'	
								}
							}};						
		localStorage.setItem('komicReserved', JSON.stringify(optionsJSON));		
	} catch (e) {	
		alert(e.message);		
	}	
}

function checkJSON(fileJSONType) {	
	if (fileJSONType === 'options') {	
		if (localStorage.getItem('komicReserved') === null) {
			jAlert(false, KomicResources.strings.jsonJS.jsonreset, '');
			resetOptionsJSON();
		}	
	}	
}

function manageJSON(action, fileJSON) {
	switch(action) {		
		case 'parse':
			return JSON.parse(fileJSON);
		
		case 'stringify':
			return JSON.stringify(fileJSON);			
	}	
}

function sortJSON(data, key) {
    return data.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });	
}