// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function KomicMenu() {    

	function setOrientation(menu) { 
		var OrientationStateText = komicReserved.komicData.liveData.orientation === 'portrait' ? KomicResources.strings.menu.orientationLandscape : KomicResources.strings.menu.orientationPortrait;
		windowing.setMenuItem(menu, 'orientation', OrientationStateText, function() {
			menu.find('li#orientation').addClass('xor');		
			var timer = setTimeout(function() {	
				if (komicReserved.komicData.liveData.orientation === 'landscape') {
					KomicKindleHub.dev.setOrientation('portrait');
					komicReserved.komicData.liveData.orientation = 'portrait';
				} else {
					KomicKindleHub.dev.setOrientation(komicReserved.komicData.options.landscape);	
					komicReserved.komicData.liveData.orientation = 'landscape';	
				}
				setIndexCSS();
			}, 300); 
		});
	};   
		
	function createKomicMenu() {
		var templateArgs = [ {
			titleText: KomicResources.strings.menu.title,            
		} ];

		var node = $('#menuKomicTemplate').tmpl(templateArgs);
		setOrientation(node);
		
		windowing.setMenuItem(node, 'bookmarks', KomicResources.strings.shared.managebookmarks, function() {
			node.find('li#bookmarks').addClass('xor'); 			
			var timer = setTimeout(function() { 
				saveKomicReserved();
				window.location = 'bookmarks.html'; 
			}, 300);
		});

		windowing.setMenuItem(node, 'deletions', KomicResources.strings.shared.scheduledeletions, function() {
			node.find('li#deletions').addClass('xor'); 						
			var timer = setTimeout(function() { 
				saveKomicReserved();
				window.location = 'deletions.html'; 
			}, 300);
		});

		windowing.setMenuItem(node, 'options', KomicResources.strings.shared.optionsmenu, function() {
			node.find('li#options').addClass('xor'); 			
			var timer = setTimeout(function() { 
				saveKomicReserved();
				window.location = 'options.html'; 
			}, 300);
		});
		
		windowing.setMenuItem(node, 'infopage', KomicResources.strings.shared.infopage, function() {
			node.find('li#infopage').addClass('xor'); 				
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.infopage, 'Komic');
			var timer = setTimeout(function() { 
				saveKomicReserved();
				window.location = 'infopage.html'; 
			}, 300);
		});
		
		windowing.setMenuItem(node, 'removeborders', KomicResources.strings.shared.removeborders, function() {
			node.find('li#removeborders').addClass('xor'); 					
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.removeborders, 'Komic');
			var timer = setTimeout(function() { 
				saveKomicReserved();
				window.location = 'borders.html'; 
			}, 300);
		});
		
		windowing.setMenuItem(node, 'screenshots', KomicResources.strings.shared.screenshots, function() {
			node.find('li#screenshots').addClass('xor'); 
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.screenshotsMoving, 'Komic');
			var timer = setTimeout(function() {
				startStopSpinningWheel('start');
				KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicscreenshot');
			}, 300);
		});
		
		windowing.setMenuItem(node, 'decompress', KomicResources.strings.shared.decompress, function() {
			node.find('li#decompress').addClass('xor'); 
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.decompressing, 'Komic');
			var timer = setTimeout(function() { 
				startStopSpinningWheel('start');
				KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicdecompress'); 
			}, 300);
		});
		
		windowing.setMenuItem(node, 'help', KomicResources.strings.shared.helppage, function() {
			node.find('li#help').addClass('xor'); 
			var timer = setTimeout(function() { 
				saveKomicReserved();
				window.location = 'help.html';
			}, 300);
		});
		
		var setLastReadStateText = KomicResources.strings.menu.lastread;
		setLastReadStateText += komicReserved.komicData.liveData.lastread;
		windowing.setMenuItem(node, 'lastRead', setLastReadStateText, null);

		node.find('.action').click(function() {
			windowing.closeMenu('index');
		});
		node.find('.close').click(function() {                                               
			windowing.closeMenu('index');                                                                   
		});	
		return node;
	};
		
	this.showKomicMenu = function() {		
		windowing.displayNodeInDialog(createKomicMenu());
	};
};