// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

var KomicResources = (function() {
	var strings = {
			version: {
				ver: 'Komic v2.3',
				shortversion: 'v2.3'
			},
			menu: {
				title: 'Menu',
				orientationLandscape: 'Switch to landscape mode',
				orientationPortrait: 'Switch to portrait mode',
				lastread: ''
			},
			bookmarkmenu: {
				title: 'Bookmark Menu',
				addtobookmark: 'Add bookmark',
				jumptobookmark: 'Jump to bookmark',
				removebookmark: 'Remove bookmark'
			},
			readingmenu: {
				title: 'Reading Menu',
				returnhome: 'Return HOME',
				choosecomic: 'Choose a comic',
				jumptopage: 'Jump to page',
				changesize: 'Change page size',
				lock: 'Lock page size',
				unlock: 'Unlock page size',
				reset: 'Reset page size',
				screenshot: 'Take a screenshot',
				addtodelete: 'Add to deletions',
				removefromdelete: 'Remove from deletions',
				scroll: 'Adjust scroll',
				autonext: 'Automatic page turner',
				contrast: 'Change contrast and brightness',
				resetcontrastbrigthness: 'Reset contrast and brightness'
			},
			colormenu: {
				title: 'Background color',
				white: 'White',
				gray: 'Gray',
				black: 'Black',
				custom: 'Choose the color',
				currentcolor: 'Current color: ',
				custommessage: 'Enter the color',
				colortitle: 'Choose the background color',
				choosecolor: 'Please choose a color'
			},
			refreshmenu: {
				title: 'Screen refresh rate',
				fivepages: 'Every 5 pages',
				tenpages: 'Every 10 pages',
				twentypages: 'Every 20 pages',
				never: 'Never',
				custom: 'Choose your value',
				currentrate: 'Current refresh rate: ',
				custommessage: 'Choose the screen refresh rate',
				refreshtitle: 'Screen refresh rate',
				chooserate: 'Please choose a value'
			},
			timermenu: {
				title: 'Automatic page turner timer',
				message: "Choose the timer",
				info: 'Express the timer in seconds',
				choosetimer: 'Please choose a timer'
			},
			optionsHTML: {
				autoSaveOption: 'Autosave',
				autoJumpOption: 'Autojump',
				zoomButtonsOption: 'Zoom buttons',
				landscapeOrientationOption: 'Landscape rotation',
				keepImageRatioOption: 'Image ratio',
				rememberOrientationOption: 'Remember mode',
				jumpToLastReadOption: 'Go to last read',
				coverViewOption: 'Cover view',
				soundsOption: 'Sounds',
				saveButtonOption: 'Fast save',
				titleBarOption: 'Status bar',
				debugMessagesOption: 'Debug messages',
				preloadOption: 'Image preloading',
				backgroundColorOption: 'Background color',
				refreshRateOption: 'Refresh rate',
				calibrationOption: 'Calibrate tap',
				calibrationOptionText0: "Touch the box",
				calibrationOptionText1: "Touch again",
				calibrationOptionText2: "Touch last time",	
				calibrationOptionText3: "Calibration done!",
				calibrationOptionAverage: "Average"
			},
			readingHTML: {
				scroll: 'Choose the scroll amount',
				value: 'Current value:',
				contrast: 'Contrast',
				brightness: 'Brightness',
				reset: 'Reset',
				confirm: 'Confirm'
			},
			infopageHTML: {
				title: 'Title',
				series: 'Series',
				number: 'Number',
				notes: 'Notes',
				year: 'Year',
				month: 'Month',
				writer: 'Writer',
				penciller: 'Penciller',
				colorist: 'Colorist',
				publisher: 'Publisher',
				genre: 'Genre',
				web: 'Link web',
				pagecount: 'Number of pages',
				languageISO: 'Language',
				communityrating: 'Community rating'
			},
			deleteJS: {
				available: 'Scheduled deletion:',
				noavailable: 'No scheduled deletion!',
				removed: 'Comic removed successfully! Please wait for the creation of the new index.',
				scheduled: 'Already scheduled!',
				popuptitleadd: 'Your comics',
				popuptitleremove: 'Your scheduled deletions',
				error: 'Error: no scheduled deletion or comic not found!',
				working: 'Deleting comics, please wait...'
			},
			bordersJS: {
				available: 'Comic to process:',
				noavailable: 'No comic to process!',
				removed: 'Borders removed successfully!',
				scheduled: 'Already scheduled!',
				popuptitleadd: 'Your comics',
				previewdone: "Now you'll see a preview of the result.<br>Tap on the image to close it",
				previewerror: 'An error occurred during the process',
				choosetolerance: 'Choose the tolerance when removing the border',
				tolerancetitle: 'Set the tolerance of the process',
				continueprocess: 'Continue with the process?',
				error: 'Error: comic not found or error during the process!',
				working: 'Removing borders, please wait...'
			},
			indexJS: {
				coverview: 'Cover view is OFF.<br>Please enable it in option menu and restart Komic',
				firstRun: 'This is your first time in Komic!<br>Please check the Help page and/or the Changelog.<br>Enjoy!',
				updated: 'You just updated Komic!<br>Please check the changelog and, if necessary, the help page.<br>Enjoy!',
				saving: 'Saving everything, please wait...'
			},
			optionJS: {
				optionsTitle0: 'Options menu 1/4',
				optionsTitle1: 'Options menu 2/4',
				optionsTitle2: 'Options menu 3/4',
				optionsTitle3: 'Options menu 4/4',
				autoSaveOption: 'Automatically save a bookmark when returning HOME or changing comic',
				autoJumpOption: 'Automatically jump to next/previous comic when reaching the end/start of a comic',
				zoomButtonsOption: 'Enable or disable zoom buttons',
				landscapeOrientationOption: 'Choose clockwise or anticlockwise rotation for landscape mode',
				keepImageRatioOption: 'Maintain image aspect ratio or stretch to full screen',
				rememberOrientationOption: 'Choose if remember last orientation used or not',
				jumpToLastReadOption: "If ON, you'll be redirected to the last read comic when launching Komic",
				coverViewOption: 'Enable or disable cover view.<br>Once inside cover view, tap on the header to return to HOME.<br>Tap on a cover to zoom it then tap it again to open the comic or tap the background to return to cover view',
				soundsOption: 'Enable or disable sounds',
				saveButtonOption: 'Enable or disable the Kindle-style touch area in the top-right angle to quickly save a bookmark',
				titleBarOption:'Enable or disable the status bar in the upper part of the screen',
				debugMessagesOption: 'Enable or disable the debug messages from shell scripts',
				preloadOption: 'Enable or disable the image preloading during reading',
				backgroundColorOption: 'Choose the background color when reading',
				refreshRateOption: 'Choose how often perform a screen full refresh',
				calibrationOption: 'Calibrate the swipe and touch gestures',
				soundsON: 'Sounds enabled',
				soundsOFF: 'Sounds disabled'
			},
			readingJS: {
				nomorecomics: 'No more comics to display!',
				lastpage: 'You are on the last page!',
				firstpage: 'You are already on the first page!',
				firstcomic: 'You are on the first comic!',
				bmrksaved: 'Bookmark saved!',
				choosepage: 'Please choose a page',
				nonexistingpage: 'You choosed a non-existing page!',
				nobmrkfolder: 'There are no bookmarks for this comic!',
				bmrkremoved: 'Bookmark removed!',
				nobmrkpage: 'There is no bookmark in this page!',
				alreadybmrk: 'Page already bookmarked!',
				savebmrkprompttitle: "Bookmark description",
				savebmrkpromptmessage: "Choose a description for your bookmark",
				nodescription: "No description",
				choosesize: 'Please choose a size',
				locked: 'Image size locked',
				unlocked: 'Image size unlocked',
				screenshot: "Screenshot taken!",
				deletionscheduled: 'Scheduled for deletion!',
				deletionremoved: 'Removed from scheduled',
				jumpprompttitle: 'Jump to image',
				jumppromptmessage: 'Enter a page number:',
				sizeprompttitle: 'Resize image',
				sizepromptmessage: 'Enter new image width in %<br>(do not enter % symbol):',
				sizeconfirmmessage: 'Change also height?',
				titlebardeletion: '--- COMIC SCHEDULED FOR DELETION ---',
				jumpfirst: 'First page',
				jumplast: 'Last page',
				currentpage: 'Current page: '
			},	
			bookmarksJS: {
				available: 'Available bookmarks:',
				popuptitle: 'Your bookmarks'
			},
			jsonJS: {
				jsonreset: "Ops, I can't find your settings...I'm going to reset them to default. Please use the option page to set them again!"
			},
			infopageJS: {
				nofile: 'ComicInfo.xml file not found for this comic! Please choose another comic.'
			},
			shared: {
				managebookmarks: 'Manage bookmarks',
				optionsmenu: 'Options menu',
				infopage: 'Comic info page',
				removeborders: 'Remove borders from comics',
				coverview: 'Cover view',
				page: 'Page: ',
				chooseacomic: 'Choose a comic',
				scheduledeletions: 'Schedule deletions',
				popupclose: 'Close',
				nobookmarksavailable: 'No bookmarks available!',
				screenshots: 'Move screenshots',
				screenshotsMoving: 'Moving screenshots, please wait...',
				screenshotsDone: 'Screenshots moved and renamed!',
				screenshotsError: 'Screenshots not found!',
				decompress: 'Decompress CBZs and CBRs',
				decompressDone: 'CBZs or CBRs decompressed. Please wait while creating the new index.',
				decompressError: 'An error occurred during decompression or no CBZ/CBR files',
				decompressing: 'Decompressing archives, please wait...',
				helppage: 'Help page',
				searchcomicmessage: 'Type something to search',
				searchcomictitle: 'Search for a comic'
			},	
			buttons: {
				choosecomic: 'Choose a Comic',
				coverview: 'Cover View',
				exit: 'Exit',
				menu: 'Menu',
				removeabookmark: 'Remove a Bookmark',
				removeall: 'Clear all',
				returnhome: 'Home',
				addto: 'Add to...',
				removefrom: 'Remove from...',
				run: 'Run',
				bookmarksmenu: 'Bookmark Menu',
				readingmenu: 'Reading Menu',
				collapse: 'Collapse',
				search: 'Search...',
				restore: 'Restore'
			}
	};  
    return {
        strings: strings
    };
} ());