// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function KomicBookmarkMenu() {    

	function createBookmarkMenu() {	
		var bookmarkPresent = checkBookmarkedPage();		
		var templateArgs = [ {
			titleText: KomicResources.strings.bookmarkmenu.title,            
		} ];
		var node = $('#menuBookmarkTemplate').tmpl(templateArgs);

		if (!bookmarkPresent) {
			windowing.setMenuItem(node, 'addbookmark', KomicResources.strings.bookmarkmenu.addtobookmark, function() {
				node.find('li#addbookmark').addClass('xor'); 
				var timer = setTimeout(function() { 
					promptSaveBookmark(); 
				}, 300);
			});
		} else {
			windowing.setMenuItem(node, 'addbookmark', KomicResources.strings.bookmarkmenu.addtobookmark, function() { });
			node.find('li#addbookmark').removeClass('action').addClass('graytext');
		}
		
		if (localStorage.getItem(komicReserved.komicData.liveData.lastread) != null) {
			windowing.setMenuItem(node, 'jumptobookmark', KomicResources.strings.bookmarkmenu.jumptobookmark, function() {
				node.find('li#jumptobookmark').addClass('xor');
				var timer = setTimeout(function() {
					var mybookmarks = fillBookmarkPopup();
					showBookmarkPopup(mybookmarks);
				}, 300);			
			});
		} else {
			windowing.setMenuItem(node, 'jumptobookmark', KomicResources.strings.bookmarkmenu.jumptobookmark, function() { });
			node.find('li#jumptobookmark').removeClass('action').addClass('graytext');
		}
		
		if (bookmarkPresent) {
			windowing.setMenuItem(node, 'removebookmark', KomicResources.strings.bookmarkmenu.removebookmark, function() {
				node.find('li#removebookmark').addClass('xor'); 
				var timer = setTimeout(function() { 
					removeThisBookmark(); 
				}, 300);
			});
		} else {
			windowing.setMenuItem(node, 'removebookmark', KomicResources.strings.bookmarkmenu.removebookmark, function() { });
			node.find('li#removebookmark').removeClass('action').addClass('graytext');
		}

		node.find('.action').click(function() {
			windowing.closeMenu('bookmark');
		});
		node.find('.close').click(function() {                               
			windowing.closeMenu('bookmark');                                                                   
		});
		return node;
	};
	
	this.showBookmarkMenu = function() {
		windowing.displayNodeInDialog(createBookmarkMenu());
	};
};