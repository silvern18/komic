// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function KomicReadingMenu() {

	function fillReadingMenu(node) {
	
		var sizeChanged = $('div.imageWrapper').data('sizechanged');
		var sizeLocked = $('div.imageWrapper').data('sizelocked');
		var imagealtered = $('div.imageWrapper').data('imagealtered');
	
		windowing.setMenuItem(node, 'returnhome', KomicResources.strings.readingmenu.returnhome, function() {
			node.find('li#returnhome').addClass('xor'); 
			var timer = setTimeout(function() { 
				returnHome(); 
			}, 300);
		});

		windowing.setMenuItem(node, 'choosecomic', KomicResources.strings.readingmenu.choosecomic, function() {
			node.find('li#choosecomic').addClass('xor');
			var timer = setTimeout(function() {
				$('div.readingButtons').hide();
				openList('reading'); 
			}, 300); 	
		});

		windowing.setMenuItem(node, 'jumptopage', KomicResources.strings.readingmenu.jumptopage, function() {
			node.find('#jumptopage').addClass('xor'); 
			var timer = setTimeout(function() { 
				promptJumpToPage(); 
			}, 300);
		});

		if (komicReserved.komicData.liveData.orientation === 'landscape') {
			windowing.setMenuItem(node, 'scroll', KomicResources.strings.readingmenu.scroll, function() {
				node.find('li#scroll').addClass('xor'); 
				var timer = setTimeout(function() {
					$('span.slidebarRange').text(komicReserved.komicData.options.scrollheight);
					$('div.slidebarBox').show();
					$('div.slidebarDiv').empty().noUiSlider('init', {
						handles: 1,
						connect: true,
						scale: [0,1050],
						start: parseInt(komicReserved.komicData.options.scrollheight),
						step: 31,
						change: function() { 
							komicReserved.komicData.options.scrollheight = $('div.slidebarDiv').noUiSlider('value')[1];
							$('span.slidebarRange').text($('.slidebarDiv').noUiSlider('value')[1]);
						}
					});
				}, 300);
			});
		}

		windowing.setMenuItem(node, 'changesize', KomicResources.strings.readingmenu.changesize, function() {
			node.find('li#changesize').addClass('xor'); 
			var timer = setTimeout(function() { 
				promptResize(); 
			}, 300);
		});

		if (sizeChanged && sizeLocked != true) {
			windowing.setMenuItem(node, 'lock', KomicResources.strings.readingmenu.lock, function() {
				node.find('li#lock').addClass('xor'); 
				var timer = setTimeout(function() { 
					lockSize(); 
				}, 300);
			});
		}
	
		if (sizeLocked) {
			windowing.setMenuItem(node, 'unlock', KomicResources.strings.readingmenu.unlock, function() {
				node.find('li#unlock').addClass('xor'); 
				var timer = setTimeout(function() { 
					unLockSize(false); 
				}, 300);
			});
		}
		
		if (sizeChanged) {
			windowing.setMenuItem(node, 'reset', KomicResources.strings.readingmenu.reset, function() {
			node.find('li#reset').addClass('xor'); 
			var timer = setTimeout(function() {
				if (sizeLocked) unLockSize(true);
				resetDimensions();
				handleCanvas('replace');
			}, 300);
		});
		}
	
		windowing.setMenuItem(node, 'screenshot', KomicResources.strings.readingmenu.screenshot, function() {
			node.find('li#screenshot').addClass('xor'); 
			var timer = setTimeout(function() { 
				screenshot();
			}, 300);
		});

		var toggleDeletionText = (checkDeletion(komicReserved.komicData.liveData.lastread)) ? KomicResources.strings.readingmenu.removefromdelete : KomicResources.strings.readingmenu.addtodelete;

		windowing.setMenuItem(node, 'toggledeletion', toggleDeletionText, function() {
			node.find('li#toggledeletion').addClass('xor'); 
			var timer = setTimeout(function() { 
				toggleDeletion(); 
			}, 300);
		});

		windowing.setMenuItem(node, 'autonext', KomicResources.strings.readingmenu.autonext, function() {
			node.find('#autonext').addClass('xor'); 
			var timer = setTimeout(function() { 
				setPromptAlertOffset('set');
				jPrompt(false, KomicResources.strings.timermenu.message, KomicResources.strings.timermenu.info, KomicResources.strings.timermenu.title, function(whichtimer) {
					if ((isNaN(whichtimer)) == true || whichtimer === '' || whichtimer === ' ') {
						jAlert(KomicResources.strings.timermenu.choosetimer, '');		
					} else if (whichtimer != null && whichtimer != '') {
						if (komicReserved.komicData.liveData.orientation === 'landscape') {
							autoNextScroll(whichtimer);	
						} else {
							autoNextPage(whichtimer);
						}						
					}
				});
				setPromptAlertOffset('reset');
			}, 300);
		});

		windowing.setMenuItem(node, 'pixastic', KomicResources.strings.readingmenu.contrast, function() {
			node.find('li#pixastic').addClass('xor'); 
			var timer = setTimeout(function() {
				if (komicReserved.komicData.options.brightnesscontrast == null) {
					komicReserved.komicData.options.brightnesscontrast = '0/0';
				}
				var values = (komicReserved.komicData.options.brightnesscontrast).split('/');
				if (values.length != 2) {
					komicReserved.komicData.options.brightnesscontrast = '0/0';
					values = (komicReserved.komicData.options.brightnesscontrast).split('/');
				}
				$('span.brightnessValueNumber').text(values[0]);
				$('span.contrastValueNumber').text(parseInt(values[1])/10);
				$('div.pixasticBox').show();
			}, 300);
		});

		if (imagealtered) {
			windowing.setMenuItem(node, 'resetcontrastbrigthness', KomicResources.strings.readingmenu.resetcontrastbrigthness, function() {
				node.find('li#resetcontrastbrigthness').addClass('xor'); 
				var timer = setTimeout(function() {
					handleCanvas('revert');
				}, 300);
			});
		}
	}

	function createReadingMenu() {
		var templateArgs = [ {
			titleText: KomicResources.strings.readingmenu.title            
		} ];
		var node = $('#menuReadingTemplate').tmpl(templateArgs);
		node.find('.action').click(function() {
			windowing.closeMenu('reading');
		});
		node.find('.close').click(function() {                               
			windowing.closeMenu('reading');                                                                  
		});
		fillReadingMenu(node);
		return node;	
	};

	this.showReadingMenu = function() {
		windowing.displayNodeInDialog(createReadingMenu());
	};
};