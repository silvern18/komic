// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function KomicColorMenu() {       
		
	function createColorMenu() {
		var templateArgs = [ {
			titleText: KomicResources.strings.colormenu.title,            
		} ];

		var node = $('#menuColorTemplate').tmpl(templateArgs);

		windowing.setMenuItem(node, 'white', KomicResources.strings.colormenu.white, function() {
			node.find('li#white').addClass('xor'); 
			komicReserved.komicData.options.backgroundcolor = 'white';
		});

		windowing.setMenuItem(node, 'gray', KomicResources.strings.colormenu.gray, function() {
			node.find('li#gray').addClass('xor');
			komicReserved.komicData.options.backgroundcolor = 'gray';
		});

		windowing.setMenuItem(node, 'black', KomicResources.strings.colormenu.black, function() {
			node.find('li#black').addClass('xor'); 
			komicReserved.komicData.options.backgroundcolor = 'black';
		});

		windowing.setMenuItem(node, 'custom', KomicResources.strings.colormenu.custom, function() {
			node.find('li#custom').addClass('xor'); 
			var timer = setTimeout(function() {
				var current = komicReserved.komicData.options.backgroundcolor;
				if (current === 'white') {
					current = KomicResources.strings.colormenu.white;
				} else if (current === 'gray') {
					current = KomicResources.strings.colormenu.gray;
				} else if (current === 'black') {
					current = KomicResources.strings.colormenu.black;
				}
				current = KomicResources.strings.colormenu.currentcolor + current;
				
				setPromptAlertOffset('set');		
				jPrompt(false, KomicResources.strings.colormenu.custommessage, current, KomicResources.strings.colormenu.colortitle, function(whichcolor) {
					if (whichcolor === '') {
						jAlert(KomicResources.strings.colormenu.choosecolor, '');		
					} else if (whichcolor != null && whichcolor != '') {
						komicReserved.komicData.options.backgroundcolor = whichcolor;		
					}
				});
				setPromptAlertOffset('reset');
			 }, 300);
		});

		node.find('.action').click(function() {
			windowing.closeMenu('reading');
		});
		node.find('.close').click(function() {                               
			windowing.closeMenu('color');                                                                   
		});  
		return node;
	};
	
	this.showColorMenu = function() {
		windowing.displayNodeInDialog(createColorMenu());
	};
};