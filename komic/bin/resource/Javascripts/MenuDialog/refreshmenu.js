// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function KomicRefreshMenu() {    

	function createRefreshMenu() {
		var templateArgs = [ {
			titleText: KomicResources.strings.refreshmenu.title,            
		} ];

		var node = $('#menuRefreshTemplate').tmpl(templateArgs);

		windowing.setMenuItem(node, '5pages', KomicResources.strings.refreshmenu.fivepages, function() {
			node.find('li#5pages').addClass('xor'); 
			komicReserved.komicData.options.refreshrate = 5; 
		});

		windowing.setMenuItem(node, '10pages', KomicResources.strings.refreshmenu.tenpages, function() {
			node.find('li#10pages').addClass('xor');
			komicReserved.komicData.options.refreshrate = 10; 	
		});

		windowing.setMenuItem(node, '20pages', KomicResources.strings.refreshmenu.twentypages, function() {
			node.find('li#20pages').addClass('xor'); 
			komicReserved.komicData.options.refreshrate = 20;
		});
		
		windowing.setMenuItem(node, 'never', KomicResources.strings.refreshmenu.never, function() {
			node.find('li#never').addClass('xor'); 
			komicReserved.komicData.options.refreshrate = 'never';
		});

		windowing.setMenuItem(node, 'custom', KomicResources.strings.refreshmenu.custom, function() {
			node.find('li#custom').addClass('xor'); 
			var timer = setTimeout(function() {
				var current = komicReserved.komicData.options.refreshrate;
				if (current === 'never') {
					current = KomicResources.strings.refreshmenu.never;
				}
				current = KomicResources.strings.refreshmenu.currentrate + current;
				
				setPromptAlertOffset('set');
				jPrompt(false, KomicResources.strings.refreshmenu.custommessage, current, KomicResources.strings.refreshmenu.refreshtitle, function(whichrate) {
					if (whichrate === '' || (isNaN(whichrate)) == true) {
						jAlert(KomicResources.strings.refreshmenu.chooserate, '');									
					} else if (whichrate != null && whichrate != '') {
						komicReserved.komicData.options.refreshrate = whichrate;			
					}
				});
				setPromptAlertOffset('reset');
			 }, 300);
		});

		node.find('.action').click(function() {
			windowing.closeMenu('refresh');
		});
		node.find('.close').click(function() {                               
			windowing.closeMenu('refresh');                                                                   
		});  
		return node;
	};

	this.showRefreshMenu = function() {
		windowing.displayNodeInDialog(createRefreshMenu());
	};
};