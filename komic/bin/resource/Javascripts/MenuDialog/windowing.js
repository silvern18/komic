// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

var windowing = new function() {
    var allCss = [];
	
	// Test if in Kindle
	var testForKindleVariable = typeof kindle != 'undefined' ? true : false;
    
    function snapshotCss(){
		// Disable if not on the Kindle
		if (!testForKindleVariable) return;
		
        jQuery.each(document.styleSheets, function(i, stylesheet) {
            var cssText = '';      
            jQuery.each(stylesheet.cssRules, function(j, rule) {
                cssText += rule.cssText;
            });
            allCss.push(cssText);
        });
    }

    this.copyAllCssToWindow = function(wnd) {   
		// Disable if not on the Kindle
		if (!testForKindleVariable) return;
		
        jQuery.each(allCss, function(i, cssText) {
            var newSheet = wnd.document.createElement('style');
            jQuery(newSheet).append(cssText);
            jQuery(wnd.document.head).append(newSheet);
        });
        // Different windows will have different ideas of the body size
        jQuery(wnd.document.body).css('width', 'auto');
        jQuery(wnd.document.body).css('height', 'auto');
    };    
    
    var currentDialog;    
    
    this.displayNodeInDialog = function(node){	
		// Hide all not visibile LIs
		node.find('li:not(.visibleLi)').hide();
		
		// Set node height
		node = setMenuHeight(node);
		
        snapshotCss();	
        var dialogTemplate = $('<div class="overlay" />').html(node);
        jQuery(document.body).append(dialogTemplate);  
		
        var width = dialogTemplate.outerWidth(true);
        var height = dialogTemplate.outerHeight(true);
        dialogTemplate.detach();

        function fillDialog(dialog){  
            windowing.copyAllCssToWindow(dialog);
            jQuery(dialog.document.body).append(dialogTemplate);
            dialog.windowing = this;
        }

        this.displayDialog('about:blank', width, height, fillDialog);

    }

    this.displayDialog = function(htmlLocation, width, height, dialogCallback) {
         var dialog = KomicKindleHub.chrome.createDialog(htmlLocation, width, height, true);
         if (!dialog) {
            myLogger.log( {
                event : 'dctpAdViwer:displayDialog',
                msg : 'Unable to create dialog window',
                level : 'error'
            });
            return;
        }
        this.closeDialog();
        currentDialog = dialog;
        jQuery(dialog).ready(dialogCallback(dialog));
    };
    
    this.closeDialog = function() {
        if (currentDialog) {
            currentDialog.close();
            currentDialog = null;
        }
    };

	this.setMenuItem = function(menuRoot, menuItemID, menuText, menuCallback) {
		var node = menuRoot.find('#' + menuItemID);
		node.addClass('visibleLi');
		if (!node) return;
		node.text(menuText);
		if (menuCallback) node.click(menuCallback);
	};
	
	this.closeMenu = function(menuType) {
		var timer = setTimeout(function() {
			windowing.closeDialog();
			if (menuType === 'reading' || menuType === 'bookmark') menuOff();
		}, 200);
	};
	
	function setMenuHeight(node) {
		var maxMenuHeight = komicReserved.komicData.liveData.orientation === 'landscape' ? 540 : 720;
		var actualHeight = node.find('li.visibleLi').length * 60;
		if (node.find('li:last-child').text().length > 43) actualHeight += 30;
		var menuHeight = actualHeight > maxMenuHeight ? maxMenuHeight : actualHeight;
		node.find('ul').height(menuHeight).css('overflowY', 'auto'); 
		return node;
	}
};