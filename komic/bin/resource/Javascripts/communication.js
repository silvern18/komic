// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function eventCallback (event, message) {

	if (event === 'communicationService') {
		if ((message == 'deletionDone') || (message == 'deletionError')){
			deleteComicsCallback(message);
		} else if ((message == 'bordersDone') || (message == 'bordersError') || (message == 'bordersPreviewDone') || (message == 'bordersPreviewError')) {
			removeBordersCallback(message);	
		} else if ((message == 'decompressDone') || (message == 'decompressError')){
			decompressCallback (message);
		} else if ((message == 'screenshotsMoved') || (message == 'screenshotsError')){
			screenshotsCallback (message);
		} else if ((message == 'arrayPageName')){
			window.open('reading.html','_self',false);
		}
		
		/* For talking GUI
		if ((message == 'nextPage')){
			nextImage ();
		} else if ((message == 'prevPage')){
			prevImage ();
		} else if ((message == 'saveBookmark')){
			saveBookmark (false, 'Vague');
		} else if ((message == 'closeKomic')){
			KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicExitNow');
		}
		*/
	}
}