// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function onLoad() {
	checkJSON('options');
	komicReserved = manageJSON('parse', localStorage.getItem('komicReserved'));
	KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.scheduledeletions, 'Komic');	
	appendText();
	fillFolders();
	fillDeletion();
	manageDeletion();
	setCSS();
	setKomicHandlers();
}
function setKomicHandlers() {
	$('button.jqueryButtons').button();
	
	$('button.addDeletionButton').click(function () {
		showDeletePopup('add');
	});
	$('button.removeDeletionButton').click(function () {
		showDeletePopup('remove');
	});
	$('button.removeAllDeletionButton').click(function () {
		removeAll();
	});
	$('button.runDeletionButton').click(function () {
		removeAndRefresh();
	});
	$('button.returnHomeButton').click(function () {
		returnHome();
	});
}
function setCSS() {
	if (komicReserved.komicData.liveData.orientation === 'portrait') {
		$('ul.scheduledDeletions').css({ 'max-height': '480px' });
	} else {
		$('ul.scheduledDeletions').css({ 'max-height': '280px' });
	}
}
function appendText () {
	$('button.addDeletionButton').text(KomicResources.strings.buttons.addto);
	$('button.removeDeletionButton').text(KomicResources.strings.buttons.removefrom);
	$('button.removeAllDeletionButton').text(KomicResources.strings.buttons.removeall);
	$('button.runDeletionButton').text(KomicResources.strings.buttons.run);
	$('button.returnHomeButton').text(KomicResources.strings.buttons.returnhome);
	
	$('span.pageHeader').text(KomicResources.strings.version.ver);	
	$('span.smallPageHeader').text(KomicResources.strings.shared.scheduledeletions);	
	$('span.scheduledDeletionsHeader').text(KomicResources.strings.deleteJS.available);
}			
function manageDeletion() {
	var $scheduledDeletions = $('ul.scheduledDeletions');
	if ($scheduledDeletions.find('li').length > 0) {			
		$scheduledDeletions.empty();
	}	
	if (localStorage.getItem('todelete') == null) {	
		$('<li>' + KomicResources.strings.deleteJS.noavailable + '</li>').appendTo($scheduledDeletions);	
	} else {
		var jsonDelete = manageJSON('parse', localStorage.getItem('todelete'));
		jsonDelete = sortJSON(jsonDelete, 'folder');
		$.each(jsonDelete, function(i, v) {
			$('<li>' + this.folder + '</li>').appendTo($scheduledDeletions);
		});
	}	
}	
function fillFolders() {
	var obj,
		myfolders = [];	
	for (var i=1; i<=numrighe; i++) {			
		var cicle = this['riga' + i];		
		if (cicle.substr(-3) == '/ !') {
			continue;
		}	
		obj = {foldername: cicle};
		myfolders.push(obj);		
	}
	if (localStorage.getItem('todelete') != null ) {
		var jsonDelete = manageJSON('parse', localStorage.getItem('todelete'));
		for (var i=0; i<=jsonDelete.length-1; i++) {
			for (var k=0; k<=myfolders.length; k++) {
				if (myfolders[k].foldername === jsonDelete[i].folder) {
      					myfolders.splice(k,1);
      					break;
   				} 
			}					
		}
	}
	return myfolders;
}
function fillDeletion() {
	var obj,
		cicle,
		mydeletions = [];
	if (localStorage.getItem('todelete') == null) {	
		obj = {foldername: KomicResources.strings.deleteJS.noavailable};
		mydeletions.push(obj);	
	} else {
		var jsonDelete = manageJSON('parse', localStorage.getItem('todelete'));
		jsonDelete = sortJSON(jsonDelete, 'folder');
		$.each(jsonDelete, function(i, v) {
			obj = {foldername: this.folder};
			mydeletions.push(obj);
		});
	}	
	return mydeletions;	
}
function addToDelete(comicToDelete) {
	var jsonDelete,
		lastId;
	if (localStorage.getItem('todelete') == null) {
		jsonDelete = [];
		lastId = 0;
	} else {
		jsonDelete = manageJSON('parse', localStorage.getItem('todelete'));
		jsonDelete = sortJSON(jsonDelete, 'id');
		lastId = parseInt(jsonDelete[jsonDelete.length-1].id) + 1;
	}
	var deletion = {'id' : lastId, 'folder' : comicToDelete};
	jsonDelete.push(deletion);		
	localStorage.setItem('todelete', manageJSON('stringify', jsonDelete));
}
function removeFromDelete(comicToRemove) {
	var jsonDelete = manageJSON('parse', localStorage.getItem('todelete'));
	for (var i = 0; i < jsonDelete.length; i++ ) {				
		if (jsonDelete[i].folder === comicToRemove) {					
			jsonDelete.splice( i, 1 );
		} 					
	}
	if (jsonDelete.length == 0) {
		localStorage.removeItem('todelete');
	} else {
		localStorage.setItem('todelete', manageJSON('stringify', jsonDelete));				
	}
}
function removeAll() {
	localStorage.removeItem('todelete');
	fillFolders();
	fillDeletion();
	manageDeletion();
}
function showDeletePopup(action) {
	var type = 'deletion';
	
	hideMyDeletion();
	showMyDeletion();
	
	$('body').append(
		'<div id="deletion_container">' +
			'<h1 id="deletion_title"></h1>' +
			'<div id="deletion_content">' +
				'<ul id="deletionList">' +
				'</ul>' +
			'</div>' +
		'</div>'
	);	
	switch(action) {
		case 'add':
			appendToDeletion(fillFolders(), action);
			break;
			
		case 'remove':
			appendToDeletion(fillDeletion(), action);
			break;
	}
	$('#deletion_container').css({
		position: 'fixed',
		zIndex: '99999',
		padding: '0',
		margin: '0'
	});
	if (action === 'add') {
		$('#deletion_title').text(KomicResources.strings.deleteJS.popuptitleadd);
	} else {
		$('#deletion_title').text(KomicResources.strings.deleteJS.popuptitleremove);
	}
	$('#deletion_content').addClass(type);
	
	switch(type) {
		case 'deletion':
			$('#deletionList').after('<div id="deletion_panel"><input type="button" id="deletion_ok" /></div>');
			$('#deletion_ok').attr('value', KomicResources.strings.shared.popupclose).click( function() {
				hideMyDeletion();
				fillFolders();
				fillDeletion();
				manageDeletion();
			});
			$('#deletion_title').click( function() {
				hideMyDeletion();
				fillFolders();
				fillDeletion();
				manageDeletion();
			});
		break;
	}
	resizeDeletion();
}
function hideMyDeletion() {
	$('#deletion_container').remove();
	$('#deletion_overlay').remove();
}
function showMyDeletion() {
	$('#deletion_overlay').remove();
	$('body').append('<div id="deletion_overlay"></div>');
	$('#deletion_overlay').css({
		position: 'absolute',
		zIndex: 99998,
		top: '0px',
		left: '0px',
		width: '100%',
		height: $(document).height(),
		background: '#FFF',
		opacity: '.01'
	});
}
function resizeDeletion() {
	var max_width = $(window).width() - 20,
		min_width = $(window).width() / 2,
		max_height = $(window).height() - 10;
		
	$('#deletion_container').css({
		'min-width': min_width + 'px',
		'max-width': max_width + 'px',
		'max-height': max_height + 'px'
	});
	
	$('#deletion_overlay').height($(document).height());
	
	var top = (($(window).height() / 2) - ($('#deletion_container').outerHeight() / 2)) + 0,
		left = (($(window).width() / 2) - ($('#deletion_container').outerWidth() / 2)) + 0;
		
	if(top < 0) top = 0;
	if(left < 0) left = 0;
	
	$('#deletion_container').css({
		'top': top + 'px',
		'left': left + 'px'
	});
}
function appendToDeletion(data, action) {
	$('#deletionList li').empty();
	for (var i=0; i<data.length; i++) {
		if (data[i] != null) {
			var foldername = data[i].foldername;
			$('<li data-foldername="' + foldername +'"><b>' + foldername + '</b></li>').appendTo( '#deletionList' );
		}
	}
	$('#deletionList li').click(function(){	
		if ($(this).data('selected') == true) {			
			$(this).css({
				'text-decoration': 'none',
				'font-style': 'normal'
			});
			$(this).data('selected', false);
			if (action === 'add') {
				removeFromDelete($(this).text());
			} else {
				addToDelete($(this).text());
			}
		} else if ($(this).data('selected') != true) {
			$(this).css({
				'text-decoration': 'underline',
				'font-style': 'italic'
			});
			$(this).data('selected', true);
			if (action === 'add') {
				addToDelete($(this).text());
			} else {
				removeFromDelete($(this).text());
			}
		}
	});	
}
function removeAndRefresh() {
	if (komicReserved != undefined) {
		localStorage.setItem('komicReserved', manageJSON('stringify', komicReserved));
	}	
	startStopSpinningWheel('start');	
	KomicKindleHub.chrome.setTitleBar(KomicResources.strings.deleteJS.working, 'Komic');
	KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicremovecomic');	
}
function removeoldbookmarks() {
	var keys = [],
		jsonDelete = manageJSON('parse', localStorage.getItem('todelete'));	
	for (var i=0; i<=localStorage.length-1; i++) {			
		keys[i] = localStorage.key(i);				
	}
	for (var i=0; i<=jsonDelete.length-1; i++) {
		for (var j=0; j<=keys.length-1; j++) {	
			if (jsonDelete[i].folder == keys[j]) {
				localStorage.removeItem(keys[j]);
			}
		}
	}	
}
function deleteComicsCallback(message) {
	startStopSpinningWheel('stop');
	switch(message) {
		case 'deletionDone':
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.scheduledeletions, 'Komic');
			removeoldbookmarks();
			localStorage.removeItem('todelete');
			jAlert(true, KomicResources.strings.deleteJS.removed, '');
			var timer = setTimeout(function() { 
				window.location.reload (true); 
				$.alerts._hide(); 
			}, 2000);
		break;
			
		case 'deletionError':
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.scheduledeletions, 'Komic');
			jAlert(false, KomicResources.strings.deleteJS.error, '');
		break;
	}
}