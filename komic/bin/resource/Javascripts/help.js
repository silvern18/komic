// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function onLoad() {
	checkJSON('options');
	KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.helppage, 'Komic');
	$('span.pageHeader').text(KomicResources.strings.version.ver);
	$('span.smallPageHeader').text('Help page');

	$('div.faq').toggle();
	$('div.help').click(function(){
		$(this).next('div.faq').toggle();
	});
	
	$('button.jqueryButtons').button();

	$('div.helpPageButtonsWrapper button.returnHome').text(KomicResources.strings.buttons.returnhome).click(function () {
		returnHome();
	});
	$('div.helpPageButtonsWrapper button.collapseAll').text(KomicResources.strings.buttons.collapse).click(function () {
		$('div.faq').hide();
	});
}

function jumpTo(destination) {
	$('div.faq').hide();
	if ( $('#'+destination).is('li') ) {
		$('#'+destination).parents('div.faq').toggle();
	} else {	
		$('#'+destination).next('div.faq').toggle();
	}
	$('#'+destination)[0].scrollIntoView(true);
	$('body').scrollTop(0)
}

function showHelpImage(direction) {
	$('body').append('<img id="helpImage" src="resource/Images/' + direction + '.png" class="centered"/>');	
	$('img#helpImage').width($(window).width());
	$('img#helpImage').height($(window).height());
	$('img#helpImage').click(function(){
		$(this).remove();
	});
}