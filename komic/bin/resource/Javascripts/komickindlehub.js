// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

KomicKindleHub = function() {
	var testForKindleVariable = typeof kindle != 'undefined' ? true : false;
	
	return {
		chrome: {
			setTitleBar: function (secondString, firstString) {
				if (testForKindleVariable) kindle.chrome.setTitleBar(secondString, firstString);
				else {
					console.log('Set Title Bar');
					console.log(firstString + ' ' + secondString);
					document.title = firstString + ' ' + secondString;
				}
			},
			createDialog: function (htmlLocation, width, height, bool) {
				if (testForKindleVariable) {
					return kindle.chrome.createDialog(htmlLocation, width, height, bool);
				} else {
					console.log('Create Dialog');
					// Create a fake dialog
					return window.open('about:blank', '_blank', 'width=' + width + ',height=' + height);
				}
			},
			createContentWindow: function(uri) {
				if (testForKindleVariable) {
					kindle.chrome.createContentWindow(uri);
					kindle.chrome.setContentWinDisplay('foreground');
					kindle.viewport.setPinchZoomState(true);
				} else {
					console.log('Create Content Window');
				}
			}
		},
		messaging: {
			sendMessage: function (sendTo, propertyName, propertyValue) {
				if (testForKindleVariable) kindle.messaging.sendMessage(sendTo, propertyName, propertyValue);
				else {
					console.log('Send Message');
					console.log(sendTo + ' ' + propertyName + ' ' + propertyValue);
				}
			},
			receiveMessage: function (event, callback) {
				if (testForKindleVariable) kindle.messaging.receiveMessage(event, callback);
				else {
					console.log('Receive Message');
					console.log(event);
				}
			}
		},		
		dev: {
			setOrientation: function (orientation) {
				if (testForKindleVariable) kindle.dev.setOrientation(orientation);
				else {
					console.log('Set Orientation');
					console.log(orientation);
					if (orientation == 'portrait') {
						$('body').width(600);
						$('body').height(800);
					} else {
						$('body').width(800);
						$('body').height(600);
					}
				}
			},
			clearCache: function () {
				if (testForKindleVariable) kindle.dev.clearCache();
				else {
					console.log('Clear cache');
				}
			},
			clearApplicationCache: function () {
				if (testForKindleVariable) kindle.dev.clearApplicationCache();
				else {
					console.log('Clear application cache');
				}
			}
		},
		appmgr: {
			onpause: function (callback) {
				if (testForKindleVariable) kindle.appmgr.onpause = callback;
				else {
					console.log('On pause');
				}
			}
		}			
	}
}();