// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function showBookmarkPopup(mybookmark) {
	var type = 'bookmark';
	
	hideMyBookmark();
	showMyBookmark();
	
	$('body').append(
		'<div id="bookmark_container">' +
			'<h1 id="bookmark_title"></h1>' +
			'<div id="bookmark_content">' +
				'<ul id="bookmarkList">' +
				'</ul>' +
			'</div>' +
		'</div>'
	);
	
	appendToBookmark(mybookmark);
	
	$('#bookmark_container').css({
		position: 'fixed',
		zIndex: '99999',
		padding: '0',
		margin: '0'
	});
	$('#bookmark_title').text(komicReserved.komicData.liveData.lastread);
	$('#bookmark_content').addClass(type);
	$('#bookmark_container').css({
		minWidth: $('#bookmark_container').outerWidth(),
		maxWidth: $('#bookmark_container').outerWidth()
	});
	repositionBookmark();
	switch( type ) {
		case 'bookmark':
			$('#bookmarkList').after('<div id="bookmark_panel"><input type="button" value="Close" id="bookmark_ok" /></div>');
			$('#bookmark_ok').attr('value', KomicResources.strings.shared.popupclose);
			$('#bookmark_ok').click( function() {
				hideMyBookmark();
				menuOff();
				callback(true);
			});
			$('#bookmark_title').click( function() {
				hideMyBookmark();
				menuOff();
				callback(true);
			});
		break;
	}
}
function hideMyBookmark() {
	$('#bookmark_container').remove();
	$('#bookmark_overlay').remove();
}
function showMyBookmark() {
	$('#bookmark_overlay').remove();
	$('body').append('<div id="bookmark_overlay"></div>');
	$('#bookmark_overlay').css({
		position: 'absolute',
		zIndex: 99998,
		top: '0px',
		left: '0px',
		width: '100%',
		height: $(document).height (),
		background: '#FFF',
		opacity: '.01'
	});
}
function repositionBookmark() {
	var top = (($(window).height() / 2) - ($('#bookmark_container').outerHeight() / 2)) - 75;
	var left = (($(window).width() / 2) - ($('#bookmark_container').outerWidth() / 2)) + 0;
	if(top < 0) top = 0;
	if(left < 0) left = 0;
				
	$('#bookmark_container').css({
		top: top + 'px',
		left: left + 'px'
	});
	$('#bookmark_overlay').height($(document).height());
}
function appendToBookmark(data) {
	$('#bookmarkList li').empty();	
	for (var i=0; i<data.length; i++) {
		if (data[i] != null) {
			var header = data[i].header;
			var page = data[i].page;
			var description = data[i].description;

			var $li = $('<li data-page="' + page + '">' + header + '<b>' + page + '</b> &nbsp &nbsp ' + description + '</li>');
			$li.appendTo('#bookmarkList');
		}
	}
	
	$('#bookmarkList li').click(function() {
		jumpToImage(parseInt($(this).data('page')), true); 
		hideMyBookmark();
	});
}