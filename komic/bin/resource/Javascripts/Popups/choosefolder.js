// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

var calledFromPage;

function openList(calledFrom) {
	var $folderList = $('div.folderList');
	calledFromPage = calledFrom;
	if ($folderList.length > 0) {
		$folderList.remove();
		if (calledFrom === 'index' || calledFrom === 'infopage') {
			var titlebarText = calledFrom === 'index' ? KomicResources.strings.version.shortversion : KomicResources.strings.shared.infopage;
			KomicKindleHub.chrome.setTitleBar(titlebarText, 'Komic');
		} else {
			$('div.menuOff, div.contextMenu').hide();
			$('div.readingButtons').show();
			updateTitleBar();
		}
		$folderList.hide();		
	} else {
		KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.chooseacomic, 'Komic');
		createHTML();
		appendToList(fillComicArray(), calledFrom);
		$('span.folderListTitle').one('click', function () {
			openList(calledFrom);
			if (calledFrom === 'infopage') $('div.infoPageButtonsWrapper').show();
		});
		$folderList.show();	
	}
}
function fillComicArray() {	
	var mycomics = [],
		currentvolume,
		currentcicle,
		hidden,
		volume,
		parent,
		parentFolderId,
		foldername,
		displayname;
	
	for (var i=1; i<=numrighe; i++) {		
		hidden = false;
		volume = false;
		parent = '';
		foldername = this['riga' + i];
		displayname = foldername;

		if (foldername.substr(-3) === '/ !') {
			// This is a volume
			foldername = foldername.substr(0, foldername.length -3);
			currentvolume = foldername;
			displayname = '+ ' + foldername;
			volume = true;
			parentFolderId = i - 1;
		} else {
			// This isn't a volume			
			currentcicle = (i == 1 || currentvolume == undefined) ? foldername : foldername.substr(0, currentvolume.length);
			if (currentcicle == currentvolume) {
				displayname = '|--->' + foldername.split('/')[1];
				hidden = true;
				parent = parentFolderId;
			}
		}
		mycomics.push({comicindex:i-1, path: foldername, name: displayname, hiddenflag: hidden, volumeflag: volume, parentfolder: parent});
	}
	return mycomics;
}
function createHTML() {
	$('body').append(
		'<div class="folderList">' +
			'<span class="folderListTitle">' + KomicResources.strings.shared.chooseacomic + '</span>' +					
			'<ul class="folderListComics"></ul>' +
			'<div class="folderListButtonsWrapper">' +
				'<button class="searchComicButton jqueryButtons">' + KomicResources.strings.buttons.search + '</button>' +
				'<button class="restoreComicButton jqueryButtons">' + KomicResources.strings.buttons.restore + '</button>' +
			'</div>' +		
		'</div>'
	);
	
	$('div.folderListButtonsWrapper button.jqueryButtons').button();
	$('div.folderListButtonsWrapper button.searchComicButton').click(function () {
		promptSearchComic();
	});
	$('div.folderListButtonsWrapper button.restoreComicButton').click(function () {
		restoreComicList();
	});
}
function appendToList(data, calledFrom) {
	var $folderListComic = $('ul.folderListComics');

	for (var i=0; i<data.length; i++) {
		if (data[i] != null) {
			var comicIndex = data[i].comicindex,
				hiddenflag = data[i].hiddenflag,
				name = data[i].name,
				path = data[i].path,
				volumeflag = data[i].volumeflag,
				parentFolderId = data[i].parentfolder;
			
			var $li = $('<li data-comicid="' + comicIndex +'" data-hidden="' + hiddenflag + '" data-volume="' + volumeflag + '" data-path="' + path + '" data-parent="' + parentFolderId + '"><b>' + name + '</b></li>');
			$li.appendTo($folderListComic);
		}
	}
	
	var $folderListComicLi = $folderListComic.find('li');
	$folderListComicLi.click(function() {
		if ($(this).data('volume') == false) {
			newOpenUp($(this).data('comicid'), data[$(this).data('comicid')].path, calledFrom); 
		} else {		
			var findings = findAll(data, $(this).text());
			var liText = $(this).text();	
			if ($(this).text().substr(0, 2) === '+ ') {
				$(this).text('- ' + (liText.substr(2, liText.length))).removeClass('closedLi').addClass('openLi');
				$folderListComicLi.filter('[data-parent="' + $(this).data('comicid') + '"]').not('.hiddenfromsearch').show();
			} else {
				$(this).text('+ ' + (liText.substr(2, liText.length))).removeClass('openLi').addClass('closedLi');
				$folderListComicLi.filter('[data-parent="' + $(this).data('comicid') + '"]').not('.hiddenfromsearch').hide();
			}
		}
	}).each(function(){
		if ($(this).data('hidden') === true) {
			$(this).hide().addClass('hiddenLi')
		}
	});
}
function findAll(source, name) {
	var findings = [];
	var k = 0;
	for (var i = 0; i < source.length; i++) {
		if ((source[i].path).indexOf(name.substr(2, name.length)) >= 0 && source[i].volumeflag == false) {
			if (source[i].comicindex >= 0) {
				findings[k] = source[i].comicindex;
				k++;
			}
		}
  	}	
	return findings;
}
function newOpenUp(index, path, from) {
	if (from === 'infopage') {
		openList('infopage');
		populateInfos(path);
		$('div.infoPageButtonsWrapper').show();
		return
	}

	if (komicReserved.komicData.options.autosave === 'ON' && from === 'reading') {
		saveBookmark(true, KomicResources.strings.optionsHTML.autoSaveOption);
	}

	komicReserved.komicData.liveData.nextFolder = parseInt(index) + 1;
	komicReserved.komicData.liveData.lastread = path;
	
	if (komicReserved != undefined) {
		localStorage.setItem('komicReserved', manageJSON('stringify', komicReserved));
	}
	
	KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicgetarray');
}
function promptSearchComic() {
	setPromptAlertOffset('set');
	jPrompt(false, KomicResources.strings.shared.searchcomicmessage, '', KomicResources.strings.shared.searchcomictitle, function(searchString) {
		if (searchString) {
			setPromptAlertOffset('reset');
			searchComic(searchString);
		}
	});
}
function searchComic(searchString) {
	var $folderListComicLi = $('ul.folderListComics li');
	$folderListComicLi.each(function () {
		var comicPath = $(this).data('path').toLowerCase();
		if (comicPath.indexOf(searchString.toLowerCase()) == -1) {
			$(this).hide().addClass('hiddenfromsearch');
		} else {
			$(this).show().removeClass('hiddenfromsearch');
		}
	});
	$folderListComicLi.filter('[data-hidden="true"]:visible').each(function () {
		var $parentLi = $folderListComicLi.filter('[data-comicid="' + $(this).data('parent') + '"]'),
			liText = $parentLi.text();
		$parentLi.text('- ' + (liText.substr(2, liText.length))).removeClass('closedLi').addClass('openLi').show();
	});
}
// se singolo, mostro il singolo
// se volume, mostro i sottocapitoli
function restoreComicList() {
	$('ul.folderListComics').empty();
	appendToList(fillComicArray(), calledFromPage);
}