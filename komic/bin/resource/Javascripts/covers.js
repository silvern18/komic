// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

var imagePerPage;

function onLoad() {
	checkJSON('options');
	
	komicReserved = manageJSON('parse', localStorage.getItem('komicReserved'));
	KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.coverview, 'Komic');
	
	imagePerPage = komicReserved.komicData.liveData.orientation === 'landscape' ? 3 : 6;
	
	var previewImageName = komicReserved.komicData.liveData.orientation === 'landscape' ? 'previewlandscape.jpg' : 'previewportrait.jpg';
	appendText();
	setKomicHandlers();
	coverView(previewImageName);
}
function setKomicHandlers() {
	$('div.coverViewButtonsWrapper img').click(function (event) {
		if ($(this).data('coverPreview')) {
			changePreviewImage($(this).attr('class'));
		} else {
			changeImageContainer($(this).attr('class'));
		}
		event.stopPropagation();
	});
	$('span.pageHeader').click(function () {
		returnHome();
	});
	$('div.container').click(function () {
		goBack();
	});
}
function appendText() {
	$('span.pageHeader').text(KomicResources.strings.version.ver);
	$('span.smallPageHeader').text(KomicResources.strings.shared.coverview);
}
function coverView(previewImageName) {	
	var imagesAdded = 0,
		containerIndex = 0,
		imageFolder,
		imageName,
		idNumber;

	$('div.imagesList').append($('<div class="currentImageContainer imageContainer' + containerIndex + '"></div>'));

	for (var i = 0; i<numrighe; i++) {
		imageFolder = this['riga' + (i+1)];
		if (imageFolder.substr(-3) != '/ !') {
			if (imagesAdded != 0 && imagesAdded % imagePerPage == 0) {
				containerIndex++;
				$('div.imagesList').append($('<div class="imageContainer' + containerIndex + '" style="display: none;"></div>'));
			}
			imageName = 'img' + imagesAdded;
			idNumber = 'myKomicId' + imagesAdded;
			imageName = document.createElement('img');
			imageName.src = '/mnt/us/documents/Komic/' + imageFolder + '/' + previewImageName;
			imageName.id = idNumber;
			$('div.imageContainer' + containerIndex).append(imageName);
			$('div.imageContainer' + containerIndex).find('img#' + idNumber).click(function (event) {
				zoomPreview($(this));
				$(this).addClass('previewing');
				event.stopPropagation();
			}).css('border', 'double 4px');
			imagesAdded++;
		}
	}
}
function zoomPreview(zoommedImage) {
	var zoommedImageId = $(zoommedImage).attr('id').replace('myKomicId', ''),
		zoomWidth = 400,
		splittedPath = unescape($(zoommedImage).attr('src')).split('/'),
		imagePath = $(zoommedImage).attr('src').replace(splittedPath[splittedPath.length-1], ''),
		firstPageName = arrayFirstImages[zoommedImageId];
		
	// Compute dimensions
	var $newImg = $('<img />');
	$newImg.attr('src', imagePath + firstPageName);
	$newImg.unbind('load');
	$newImg.bind('load', function () {
		var image_height = this.height,
			image_width = this.width,
			zoomHeight = zoomWidth * (image_height / image_width);			
		$('div.imagesList, span.pageHeader, span.smallPageHeader').hide();
		$('div.coverViewButtonsWrapper img').data('coverPreview', true);
		$('img.previewImage').attr('src', imagePath + firstPageName).css({
			'width': zoomWidth,
			'height': zoomHeight		
		}).addClass('centered').show().click(function () {
			openUp(zoommedImageId, imagePath);
		});
		$(this).remove();
	});
}
function openUp(zoommedImageId, imagePath) {
	komicReserved.komicData.liveData.nextFolder = parseInt(zoommedImageId) + 1;
	komicReserved.komicData.liveData.lastread = imagePath.replace('/mnt/us/documents/Komic/', '').slice(0, -1);	
	if (komicReserved != undefined) {
		localStorage.setItem('komicReserved', manageJSON('stringify', komicReserved));
	}	
	sessionStorage.clear();	
	KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicgetarray');	
}
function goBack() {
	$('div.imagesList, span.pageHeader, span.smallPageHeader').show();
	$('div.coverViewButtonsWrapper img').removeData('coverPreview');
	$('img.previewImage').hide();
	$('div.imagesList img.previewing').removeClass('previewing').closest('div').addClass('currentImageContainer').show().siblings('div').removeClass('currentImageContainer').hide();
}
function changeImageContainer(buttonClass) {
	var $imageContainers = $('div.imagesList div'),
		currentImageContainer = $('div.imagesList').find('div.currentImageContainer').index(),
		maxImageContainer = $('div.imagesList div').length - 1;
	
	if (buttonClass === 'rightArrowButton') {
		currentImageContainer++;
	} else {
		currentImageContainer--;
	}
	
	if (currentImageContainer > maxImageContainer) currentImageContainer = 0;
	
	$imageContainers.removeClass('currentImageContainer');	
	$imageContainers.eq(currentImageContainer).show().addClass('currentImageContainer');
	$imageContainers.not('.currentImageContainer').hide();
}
function changePreviewImage(buttonClass) {
	var $imageList = $('div.imagesList'),
		$previewImage = $imageList.find('img.previewing'),
		currentImageIndex = parseInt($previewImage.attr('id').replace('myKomicId', '')),
		maxImageNumber = $('div.imagesList img').length - 1;
	
	if (buttonClass === 'rightArrowButton') {
		currentImageIndex++;
	} else {
		currentImageIndex--;
	}
	
	if (currentImageIndex > maxImageNumber) currentImageIndex = 0;
	else if (currentImageIndex < 0) currentImageIndex = maxImageNumber;
	
	$previewImage.removeClass('previewing');
	$imageList.find('img#myKomicId' + currentImageIndex).click();
}