// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function onLoad() {	
	checkJSON('options');
	KomicKindleHub.chrome.setTitleBar(KomicResources.strings.version.shortversion, 'Komic');

	appendText();	
	setIndexCSS();
	setKomicHandlers();
	setupChrome();
	firstRunOrUpdate();
	setSplashImage();
	
	// Hide/show titlebar if launched from searchbar shortcut
	if (komicReserved.komicData.options.komictitlebar === 'OFF') {
		KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komictitlebarOFF');
	}

	// If from a komic page
	if (unPackData()[0] === 'false') {
		if (komicReserved.komicData.liveData.orientation === 'landscape') {
			if (komicReserved.komicData.options.landscape === 'landscapeRight') {
				KomicKindleHub.dev.setOrientation('landscapeRight');
			} else {
				KomicKindleHub.dev.setOrientation('landscapeLeft');
			}
		}
	} else {
		if (komicReserved.komicData.options.rememberMode === 'ON') {
			if (komicReserved.komicData.liveData.orientation === 'landscape') {
				if (komicReserved.komicData.options.landscape === 'landscapeRight') {	
					KomicKindleHub.dev.setOrientation('landscapeRight');
				} else {
					KomicKindleHub.dev.setOrientation('landscapeLeft');
				}
			}
		} else {		
			komicReserved.komicData.liveData.orientation = 'portrait';
		}
		
		// Remember last read
		if (komicReserved.komicData.options.rememberLastRead === 'ON') {
			for (var i = 1; i <= numrighe; i++)  {
				if ( (this['riga' + i]) == komicReserved.komicData.liveData.lastread ) {
					saveKomicReserved();
					window.open('reading.html','_self',false);
					return;
				}
			}
		}
	}
}
function firstRunOrUpdate() {
	var komicStatus = localStorage.getItem('komicStatus');
	if (komicStatus) {
		if (komicStatus === 'firstRun') {
			jAlert(false, KomicResources.strings.indexJS.firstRun, ''); 
		} else if (komicStatus === 'updated') {
			jAlert(false, KomicResources.strings.indexJS.updated, '');
		}
		localStorage.removeItem('komicStatus');
	}
}
function setIndexCSS() {
	if (komicReserved.komicData.liveData.orientation === 'landscape') {
		$('div.indexButtonsWrapper').css('marginTop', '30px');
		if (komicReserved.komicData.options.komictitlebar === 'OFF') {
			$('div.indexButtonsWrapper button.coverViewButton, div.indexButtonsWrapper button.komicMenuButton').css('marginTop', '30px');
			$('div.indexButtonsWrapper button.exitKomicButton').css('marginTop', '30px');
		} else {
			$('div.indexButtonsWrapper button.coverViewButton, div.indexButtonsWrapper button.komicMenuButton').css('marginTop', '15px');
			$('div.indexButtonsWrapper button.exitKomicButton').css('marginTop', '15px');
		}
	} else {
		$('div.indexButtonsWrapper').css('marginTop', '100px');
		$('div.indexButtonsWrapper button.coverViewButton, div.indexButtonsWrapper button.komicMenuButton').css('marginTop', '40px');
		$('div.indexButtonsWrapper button.exitKomicButton').css('marginTop', '40px');
	}
}
function setKomicHandlers() {
	$('button.jqueryButtons').button();
	
	$('button.chooseFolderButton').click(function () {
		openList('index');
	});
	$('button.coverViewButton').click(function () {
		openCoverView();
	});
	$('button.komicMenuButton').click(function () {
		new KomicMenu().showKomicMenu();
	});
	$('button.exitKomicButton').click(function () {
		KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicExitNow');
	});
}
function appendText() {
	$('button.chooseFolderButton').text(KomicResources.strings.buttons.choosecomic);
	$('button.coverViewButton').text(KomicResources.strings.buttons.coverview);
	$('button.komicMenuButton').text(KomicResources.strings.buttons.menu);
	$('button.exitKomicButton').text(KomicResources.strings.buttons.exit);
//	$('span.pageHeader').text(KomicResources.strings.version.ver);
}
function openCoverView() {
	if (komicReserved.komicData.options.preview === 'ON') {
		saveKomicReserved();
		window.open('covers.html','_self',false);
	} else {
		jAlert(false, KomicResources.strings.indexJS.coverview, '');
	}
}
function setSplashImage() {
	// Add index splash image
	$('img.splashImage').click(function () {
		$('body').append('<img id="newIndex" src="resource/Images/landingImage.png" class="centered" usemap="#myImageMap" />');	
		$('img#newIndex').width($(window).width()).height($(window).height()).click(function(){
			$(this).remove();
		});
	});

	$('map#myImageMap area').click(function(e) {
		var thisTitle = $(this).attr('title');
		switch (thisTitle) {
			case 'decompressArchives':
				KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.decompressing, 'Komic');
				startStopSpinningWheel('start');
				KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicdecompress'); 
			break;
			
			case 'manageScreenshots':
				KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.screenshotsMoving, 'Komic');
				startStopSpinningWheel('start');
				KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicscreenshot');
			break;
			
			case 'removeBorders':
				KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.removeborders, 'Komic');
				saveKomicReserved();
				window.location = 'borders.html';
			break;
			
			case 'lastRead':
				jAlert(false, komicReserved.komicData.liveData.lastread, '');
			break;
			
			case 'showCredits':
				jAlert(false, 'Not yet implemented', '');
			break;
			
			case 'exitKomic':
				KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicExitNow');
			break;
			
			case 'openComic':
				$('img#newIndex').remove();
				openList('index');
			break;
			
			case 'deletionsPage':
				saveKomicReserved();
				window.location = 'deletions.html';
			break;
			
			case 'infoPage':
				KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.infopage, 'Komic');
				saveKomicReserved();
				window.location = 'infopage.html';
			break;
			
			case 'coverView':
				openCoverView();
			break;
			
			case 'changeOrientation':
				$('img#newIndex').remove();
				if (komicReserved.komicData.liveData.orientation === 'landscape') {
					KomicKindleHub.dev.setOrientation('portrait');
					komicReserved.komicData.liveData.orientation = 'portrait';						
				} else {
					KomicKindleHub.dev.setOrientation(komicReserved.komicData.options.landscape);	
					komicReserved.komicData.liveData.orientation = 'landscape';	
				}
			break;
			
			case 'bookmarksPage':
				saveKomicReserved();
				window.location = 'bookmarks.html'; 
			break;
			
			case 'optionsPage':
				saveKomicReserved();
				window.location = 'options.html';
			break;
			
			case 'helpPage':
				if (komicReserved.komicData.liveData.orientation === 'portrait') {
					saveKomicReserved();
					window.location = 'help.html';
				}
			break;		
		}	
	});
}