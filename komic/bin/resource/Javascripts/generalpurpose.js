// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

var data = new Array('false');	// data[0]=redirect

var komicReserved = (function() {
	var komicOptionsAndData = manageJSON('parse', localStorage.getItem('komicReserved'));
	return komicOptionsAndData;
} ());
function saveKomicReserved() {
	if (komicReserved) {
		localStorage.setItem('komicReserved', manageJSON('stringify', komicReserved));
	}
}
function startStopSpinningWheel(action) {
	switch(action) {
		case 'start':
			$('body').append("<img class='spinningWheel' src='resource/Images/working.gif'/>");
			$('img.spinningWheel').show();
		break;		
		case 'stop':
			$('img.spinningWheel').remove();
		break;
	}
}
function menuButtonCallback(property, json){
	if (property === 'searchBarButtonSelected') {
		if (json === 'menu') {
		} else if (json === 'back') {
		} else if (json === 'store') {
		}	
	} else if (property === 'systemMenuItemSelected') {
		if (json === 'changelog') {
			$.alerts.verticalOffset = 20;
			$('body').click(function(){
				$.alerts._hide();
			});
			var changeLog = '';
			jAlert(false, changeLog, '');
		} else if (json === 'credits') {
			$('body').click(function(){
				$.alerts._hide();
			});
			var thanksTo = '';
			jAlert(false, thanksTo, '');
		} else if (json === 'help') {
		}
	}
}
function setupChrome() {
	var json = {
            "appId": "com.silver18.komic001",            
            "searchBar": {
                "clientParams": {
                    "profile": {
                        "name": "komicprofile",
                        "buttons": [
                            { 
                                "id": "menu",
                                "state": "enabled",
                                "handling":"system"
                            },                             
                            { 
                                "id": "back",
                                "state": "hidden",
                                "handling":"notifyApp" 
                            }, 
                            { 
                                "id": "store",
                                "state": "hidden",
                                "handling":"notifyApp"
                            },
							{ 
								"id": "home", 
								"state": "enabled", 
								"handling": "system" 
							}
                        ]
                    }
                } 
            }, 
            "systemMenu": {
                "clientParams": {
                    "profile": {
                        "name": "default",
                        "items": [
                            { 
                                "id": "store",
                                "state": "hidden",
                                "handling":"system",
                                "label":"The damn store",
                                "position":0
                            },
							{ 
                                "id": "changelog",
                                "state": "enabled",
                                "handling":"notifyApp",
								"label":"ChangeLog Komic v2.3",
                                "position":1
                            },
							{ 
                                "id": "credits",
                                "state": "enabled",
                                "handling":"notifyApp",
                                "label":"Credits",
                                "position":2
                            }
                        ],
                        "selectionMode": "none",
                        "closeOnUse": true
                        
                    }
                } 
            } 
        };
        KomicKindleHub.messaging.sendMessage('com.lab126.pillow', 'configureChrome', json);
}
function decompressCallback(message) {
	switch(message) {
		case 'decompressDone':
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.version.shortversion, 'Komic');
			startStopSpinningWheel('stop');
			jAlert(true, KomicResources.strings.shared.decompressDone, "");
			var timer = setTimeout(function() { window.location.reload (true); $.alerts._hide() }, 4000);
		break;
			
		case 'decompressError':
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.version.shortversion, 'Komic');
			startStopSpinningWheel('stop');
			jAlert(false, KomicResources.strings.shared.decompressError, "");
		break;
	}
}
function screenshotsCallback(message) {
	switch(message) {
		case 'screenshotsMoved':
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.version.shortversion, 'Komic');
			startStopSpinningWheel('stop');
			jAlert(true, KomicResources.strings.shared.screenshotsDone, "");
			var timer = setTimeout(function() { window.location.reload (true); $.alerts._hide() }, 1500);
		break;
			
		case 'screenshotsError':
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.version.shortversion, 'Komic');
			startStopSpinningWheel('stop');
			jAlert(false, KomicResources.strings.shared.screenshotsError, "");
		break;
	}
}
function exitKomicNow() {
	// Check if reading and save bookmark
	var pageUrl = window.location.pathname.split('/');
	if (pageUrl[pageUrl.length-1] === 'reading.html' && komicReserved.komicData.options.autosave === 'ON') {
		saveBookmark(true, KomicResources.strings.optionsHTML.autoSaveOption);
	}
	
	// Save komic options
	if (komicReserved != undefined) {
		localStorage.setItem('komicReserved', manageJSON('stringify', komicReserved));
	}
	
	KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komictitlebarON');

	// I need to find something blocking...
	alert(KomicResources.strings.indexJS.saving);	
}
function packData() {
	var packed = "";	
  	for (var i=0; (i<data.length); i++) {
		if (i>0) {
			packed += ",";
		}
		packed += escape(data[i]);
  	}	
	return packed;
}
function unPackData() {
	var query = window.location.search;

	if (query.substring(0, 1) == '?') {
    	query = query.substring(1);
  	}
  	data = query.split(','); 
  	for (var i = 0; (i < data.length); i++) {
    	data[i] = unescape(data[i]);
  	}
	return data
}
function returnHome(flagSaveBookmark) {	
	// If saveBookmark is not false, autosave is ON and we are in reading, save bookmark
	if (flagSaveBookmark != false && komicReserved.komicData.options.autosave === 'ON' && $('div.readingWrapper').length > 0) { 
		saveBookmark(true, KomicResources.strings.optionsHTML.autoSaveOption);
	}
	// Save preferences, clear sessionStorage and return home
	saveKomicReserved();
	sessionStorage.clear();
	window.location = 'index.html?' + packData();
}
function setPromptAlertOffset(action) {
	switch(action) {
		case 'set':
			if (komicReserved.komicData.liveData.orientation === 'landscape') {
				$.alerts.verticalOffset = -950;
			} else {
				$.alerts.verticalOffset = -220;
			}
		break;
		case 'reset':
			$.alerts.verticalOffset = -75;
		break;
	}
}
function getScrollbarWidth() {
    var outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.width = '100px';
    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    outer.style.overflow = 'scroll';
	
    var inner = document.createElement('div');
    inner.style.width = '100%';
    outer.appendChild(inner);        

    var widthWithScroll = inner.offsetWidth;

    outer.parentNode.removeChild(outer);
    return widthNoScroll - widthWithScroll;
}