// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function onLoad() {
	checkJSON('options');
	komicReserved = manageJSON('parse', localStorage.getItem('komicReserved'));

	appendText();
	fillFolders();
	manageProcess();
	setCSS();
	setKomicHandlers();
	
	KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.removeborders, 'Komic');	

	if (sessionStorage.getItem('borderPreview') === 'true') {
		sessionStorage.removeItem('borderPreview');
		showBorderPreview();
		return;
	}
}
function setKomicHandlers() {
	$('button.jqueryButtons').button();
	
	$('button.addBorderButton').click(function () {
		showBordersPopup();
	});
	$('button.removeBorderButton').click(function () {
		removeAll();
	});
	$('button.runBorderButton').click(function () {
		previewResults();
	});
	$('button.returnHomeButton').click(function () {
		returnHome();
	});
}
function setCSS() {
	if (komicReserved.komicData.liveData.orientation === 'portrait') {
		$('ul.scheduledBorders').css({ 'max-height': '335px' });
	} else {
		$('ul.scheduledBorders').css({ 'max-height': '190px' });
	}
}
function appendText() {
	$('button.addBorderButton').text(KomicResources.strings.buttons.addto);
	$('button.removeBorderButton').text(KomicResources.strings.buttons.removefrom);
	$('button.runBorderButton').text(KomicResources.strings.buttons.run);
	$('button.returnHomeButton').text(KomicResources.strings.buttons.returnhome);
	$('span.pageHeader').text(KomicResources.strings.version.ver);
	$('span.smallPageHeader').text(KomicResources.strings.shared.removeborders);
	$('span.scheduledBordersHeader').text(KomicResources.strings.bordersJS.available);
}			
function manageProcess() {			
	var $scheduledBorders = $('ul.scheduledBorders');
	
	if ($scheduledBorders.find('li').length > 0) {			
		$scheduledBorders.empty();
	}
	
	if (localStorage.getItem('removeborders') == null) {			
		$('<li>' + KomicResources.strings.bordersJS.noavailable + '</li>').appendTo($scheduledBorders);			
	} else {
		$('<li>' + manageJSON('parse', localStorage.getItem('removeborders')).folder + '</li>').appendTo($scheduledBorders);
	}
}
function fillFolders() {
	var myfolders = [];

	for (var i=1; i<=numrighe; i++) {			
		var cicle = this['riga' + i];
		if (cicle.substr(-3) === '/ !') continue;
		myfolders.push({foldername: cicle});	
	}

	if (localStorage.getItem('removeborders') != null ) {
		var folderToProcess = manageJSON('parse', localStorage.getItem('removeborders'));
		for (var i=0; i<=myfolders.length; i++) {
			if (myfolders[i].foldername === folderToProcess.folder) {
					myfolders.splice(i,1);
					break;
			} 
		}							
	}
	
	return myfolders;
}
function removeAll() {
	localStorage.removeItem('removeborders');
	fillFolders();
	manageProcess();
}
function showBordersPopup() {
	var type = 'borders';

	hideMyBorders();
	showMyBorders();
	
	$('body').append(
		'<div id="borders_container">' +
			'<h1 id="borders_title"></h1>' +
			'<div id="borders_content">' +
				'<ul id="bordersList">' +
				'</ul>' +
			'</div>' +
		'</div>'
	);

	appendToBorders(fillFolders());

	$('#borders_container').css({
		position: 'fixed',
		zIndex: '99999',
		padding: '0',
		margin: '0'
	});

	$('#borders_title').text(KomicResources.strings.bordersJS.popuptitleadd);
	$('#borders_content').addClass(type);

	switch(type) {
		case 'borders':
			$('#bordersList').after('<div id="borders_panel"><input type="button" id="borders_ok" /></div>');
			$('#borders_ok').attr('value', KomicResources.strings.shared.popupclose).click( function() {
				hideMyBorders();
				fillFolders();
				manageProcess();
			});
			$('#borders_title').click( function() {
				hideMyBorders();
				fillFolders();
				manageProcess();
			});
		break;
	}
	resizeBorders();
}
function hideMyBorders() {
	$('#borders_container').remove();
	$('#borders_overlay').remove();
}
function showMyBorders() {
	$('#borders_overlay').remove();
	$('body').append('<div id="borders_overlay"></div>');
	$('#borders_overlay').css({
		position: 'absolute',
		zIndex: 99998,
		top: '0px',
		left: '0px',
		width: '100%',
		height: $(document).height(),
		background: '#FFF',
		opacity: '.01'
	});
}
function resizeBorders() {
	var $bordersContainer = $('#borders_container');

	var max_width = ($(window).width()) - 20,
		min_width = ($(window).width()) / 2,
		max_height = ($(window).height()) - 10;
				
	$bordersContainer.css({
		'min-width': min_width + 'px',
		'max-width': max_width + 'px',
		'max-height': max_height + 'px'
	});

	$('#borders_overlay').height( $(document).height() );

	var top = (($(window).height() / 2) - ($bordersContainer.outerHeight() / 2)) + 0;
	var left = (($(window).width() / 2) - ($bordersContainer.outerWidth() / 2)) + 0;

	if( top < 0 ) top = 0;
	if( left < 0 ) left = 0;

	$bordersContainer.css({
		'top': top + 'px',
		'left': left + 'px'
	});
}
function appendToBorders(data) {
	$('#bordersList li').empty();
	for (var i=0; i<data.length; i++) {
		if (data[i] != null) {
			$('<li><b>' + data[i].foldername + '</b></li>').appendTo('#bordersList');
		}
	}
	$('#bordersList li').click(function(){
		$(this).css({
			'text-decoration': 'underline',
			'font-style': 'italic'
		});
		localStorage.setItem('removeborders', JSON.stringify({'folder': $(this).text(), 'tolerance': 4}));
		hideMyBorders();
		fillFolders();
		manageProcess();
	});	
}
function previewResults() {
	if (localStorage.getItem('removeborders') == null) {
		jAlert(false, KomicResources.strings.bordersJS.noavailable, '');
	} else {
		setPromptAlertOffset('set');
		// Open prompt
		jPrompt(false, KomicResources.strings.bordersJS.choosetolerance, '', KomicResources.strings.bordersJS.tolerancetitle, function(tolerance) {
			if (tolerance != null && (isNaN(tolerance)) == false && tolerance != '') {
				startStopSpinningWheel('start');
				setPromptAlertOffset('reset');
				
				// Save folder and tolerance
				var folderToProcess = manageJSON('parse', localStorage.getItem('removeborders'));
				localStorage.setItem('removeborders', JSON.stringify({'folder': folderToProcess.folder, 'tolerance': tolerance}));

				// Start the script
				var timer = setTimeout(function() { 
					KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicremoveborderpreview'); 
				}, 2000);
			}
		});	
	}
}
function showBorderPreview() {	
	// Append the image with its source
	$('div.borderPreviewBox').append('<img id="borderPreviewImage"/>');
	var $borderPreviewImage = $('img#borderPreviewImage');
	$borderPreviewImage.attr('src', borderPreviewImagePath);
	
	var newImg = new Image();
	newImg.src = $borderPreviewImage.attr('src');

	// Compute the dimensions
	var image_height = newImg.height;
	var image_width = newImg.width;

	var larghezza = 500;
	var altezza = larghezza*(image_height/image_width);
	
	$borderPreviewImage.height(altezza);
	$borderPreviewImage.width(larghezza);
	
	// Center the image and show it
	$borderPreviewImage.click(closePreview);
	$borderPreviewImage.addClass('centered').css('border', '5px double black');
	$('div.container').hide();
	$('div.borderPreviewBox').show();
}
function closePreview() {
	$('div.borderPreviewBox').hide().empty();
	$('div.container').show();
	setPromptAlertOffset('reset');
	jConfirm(KomicResources.strings.bordersJS.continueprocess, '', function(response) {
		if (response == true) {
			removeAndRefresh();
		}	
	});	
}
function removeAndRefresh () {
	if (komicReserved != undefined) {
		localStorage.setItem('komicReserved', manageJSON('stringify', komicReserved));
	}
	startStopSpinningWheel('start');
	KomicKindleHub.chrome.setTitleBar(KomicResources.strings.bordersJS.working, 'Komic');
	KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicremoveborder');
}
function removeBordersCallback(message) {
	startStopSpinningWheel('stop');
	switch(message) {
		case 'bordersDone':
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.removeborders, 'Komic');
			localStorage.removeItem('removeborders');
			jAlert(true, KomicResources.strings.bordersJS.removed, '');
			var timer = setTimeout(function() { 
				KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicrefreshscreen'); 
				window.location.reload(true); 
				$.alerts._hide(); 
			}, 2000);
		break;
		
		case 'bordersError':
			KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.removeborders, 'Komic');
			var timer = setTimeout(function() { 
				KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicrefreshscreen'); 
			}, 500);
			jAlert(false, KomicResources.strings.bordersJS.error, '');
		break;
		
		case 'bordersPreviewDone':
			jAlert(true, KomicResources.strings.bordersJS.previewdone, '');	
			var timer = setTimeout(function() { 
				$.alerts._hide();
				KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicrefreshscreen');
				sessionStorage.setItem('borderPreview', true);
				window.location.reload(true); 
			}, 4000);
		break;
		
		case 'bordersPreviewError':
			KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicrefreshscreen');
			jAlert(false, KomicResources.strings.bordersJS.previewerror, '');
			var timer = setTimeout(function() { 
				$.alerts._hide(); 
			}, 4000);
		break;
	}
}