// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function onLoad() {
	checkJSON('options');
	komicReserved = manageJSON('parse', localStorage.getItem('komicReserved'));
	if (komicReserved.komicData.liveData.orientation == 'landscape') {	
		$('span.smallPageHeader').hide();
		$('div.infoList').css({ 
			height: '67%', 
			top: '13%' 
		});	
	}	
	appendText();
	openList('infopage');	
	
	$('button.jqueryButtons').button();	
	$('div.infoPageButtonsWrapper button.chooseFolder').text(KomicResources.strings.buttons.choosecomic).click(function () {
		openList('infopage');
	});
	
	$('div.infoPageButtonsWrapper button.returnHome').text(KomicResources.strings.buttons.returnhome).click(function () {
		returnHome();
	});
}
function populateInfos(url) {
	$('ul.infoIndex').empty();
	var url = '/mnt/us/documents/Komic/' + url + '/ComicInfo.xml';
	var comicInfos = [];
	
	var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
	xhttp.open('GET', url, false);
	xhttp.send();
	var xmlDoc = xhttp.responseXML;	
	if (xmlDoc == null) {
		jAlert(true, KomicResources.strings.infopageJS.nofile, '');
		var timer = setTimeout(function() { 
			openList('infopage'); 
			$.alerts._hide() 
		}, 2000);
		return;
	}
	
	var obj = {
		Title: xmlDoc.getElementsByTagName("Title")[0].childNodes[0].textContent, 
		Series: xmlDoc.getElementsByTagName("Series")[0].childNodes[0].textContent, 
		Number: xmlDoc.getElementsByTagName("Number")[0].childNodes[0].textContent, 
		Notes: xmlDoc.getElementsByTagName("Notes")[0].childNodes[0].textContent, 
		Year: xmlDoc.getElementsByTagName("Year")[0].childNodes[0].textContent,
		Month: xmlDoc.getElementsByTagName("Month")[0].childNodes[0].textContent,
		Writer: xmlDoc.getElementsByTagName("Writer")[0].childNodes[0].textContent,
		Penciller: xmlDoc.getElementsByTagName("Penciller")[0].childNodes[0].textContent,
		Colorist: xmlDoc.getElementsByTagName("Colorist")[0].childNodes[0].textContent,
		Publisher: xmlDoc.getElementsByTagName("Publisher")[0].childNodes[0].textContent,
		Genre: xmlDoc.getElementsByTagName("Genre")[0].childNodes[0].textContent,
		Web: xmlDoc.getElementsByTagName("Web")[0].childNodes[0].textContent,
		PageCount: xmlDoc.getElementsByTagName("PageCount")[0].childNodes[0].textContent,
		LanguageISO: xmlDoc.getElementsByTagName("LanguageISO")[0].childNodes[0].textContent,
		CommunityRating: xmlDoc.getElementsByTagName("CommunityRating")[0].childNodes[0].textContent
	};
	comicInfos.push(obj);	
	$('#infoTemplate').tmpl(comicInfos).appendTo('ul.infoIndex');	
}	
function appendText() {	
	$('span.pageHeader').text(KomicResources.strings.version.ver);
	$('span.smallPageHeader').text(KomicResources.strings.shared.infopage);
}