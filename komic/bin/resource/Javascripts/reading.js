// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

var offsetXimage;
var offsetYimage;
var mouseYinitpos;
var mouseXinitpos;
var start;

var zoom = 1;
var flagSwiped = false;
var preloadedDimensions = new Object();

function storeMostUsedVariables() {
	$imageWrapper = $('div.imageWrapper');
	$boxToHide = $('div.slidebarBox, div.pixasticBox');
	newImg = new Image();
}
function start() {
	// Clear any cached file and load the new file list
	externalfunctions('memory');
	setKomicHandlers();
	
	checkJSON('options');
	sessionStorage.setItem('pagesgone', 0);

	// Check for bookmarks
	if (!localStorage.getItem(komicReserved.komicData.liveData.lastread)) {
		komicReserved.komicData.liveData.pagenum = '0';
	} else {	
		// We check the last bookmarked page sorting the array by page
		var jsonBookmarks = manageJSON('parse', localStorage.getItem(komicReserved.komicData.liveData.lastread));
		jsonBookmarks = sortJSON(jsonBookmarks, 'page');
		komicReserved.komicData.liveData.pagenum = parseInt(jsonBookmarks[jsonBookmarks.length-1].page);	
	}
	
	// Set image src
	$imageWrapper.empty().html('<img id="image" class="image" src="/mnt/us/documents/Komic/' + escape(komicReserved.komicData.liveData.lastread) + '/' + arrayPageName[parseInt(komicReserved.komicData.liveData.pagenum)] + '"/>');
	$imageWrapper.find('img.image').load(function(){
		var dimensions = getNaturalDimensions($(this));
		preloadedDimensions[arrayPageName[parseInt(komicReserved.komicData.liveData.pagenum)]]= {width: dimensions.width, height: dimensions.height};
		setActiveImage('image');	
	});
	
	appendText();
	setCSS();
	preLoadNext();
	setExtendedInfo(false, true);
	setKomicBackground();
	setKomicZoomButtons();
	setKomicSaveButton();
	getCalibrationData();
}
function setKomicHandlers() {
	// Buttons
	$('button.jqueryButtons').button();

	// Reading menu
	$('button.readingMenu').click(function () {
		new KomicReadingMenu().showReadingMenu();
	});
	// Bookmark menu
	$('button.bookmarkMenu').click(function () {
		new KomicBookmarkMenu().showBookmarkMenu();
	});
	// Buttons
	$('div.prevPage, div.nextPage, div.menuOn, div.zoomIn, div.zoomOut, div.saveBookmark, div.scrollDown')
	.mousemove(function () {
		trackMouse();
	})
	.mouseup(function () {
		mouseUpCallback($(this).index());
	});
	// Close menu
	$('div.menuOff').click(function () {
		menuOff();
	});	
	// Close slidebar box
	$('span.slidebarText').click(function () {
		$(this).closest('div.slidebarBox').hide();
	});
	// Close pixastic box
	$('span.contrastText, span.brightnessText').click(function () {
		$(this).closest('div.pixasticBox').hide();
	});
	// Contrast and brightness values
	$('div.pixasticBox img').click(function () {
		showValuePostprocessing($(this).data('action'), $(this).data('value'));
	});
	// Confirm and reset pixastic
	$('td.resetText').click(function () {
		handleCanvas('reset');
	});
	$('td.confirmText').click(function () {
		setContrastBrightness(true);
	});
}
function appendText() {	
	$('button.readingMenu').text(KomicResources.strings.buttons.readingmenu);
	$('button.bookmarkMenu').text(KomicResources.strings.buttons.bookmarksmenu);
	$('span.slidebarText').text(KomicResources.strings.readingHTML.scroll);
	$('span.slidebarValue').text(KomicResources.strings.readingHTML.value);
	$('span.contrastText').text(KomicResources.strings.readingHTML.contrast);
	$('span.contrastValueText').text(KomicResources.strings.readingHTML.value);
	$('span.brightnessText').text(KomicResources.strings.readingHTML.brightness);
	$('span.brightnessValueText').text(KomicResources.strings.readingHTML.value);
	$('td.resetText').text(KomicResources.strings.readingHTML.reset);
	$('td.confirmText').text(KomicResources.strings.readingHTML.confirm);
}
function setKomicBackground() {
	$('body').css('background-color', komicReserved.komicData.options.backgroundcolor);
}
function setCSS() {
	// Media queries could help here...
	if (komicReserved.komicData.liveData.orientation === 'landscape') {
		$('div.scrollDown').show();
		$('div.readingWrapper').css({ height: '1067px' });
	}
}
function setKomicZoomButtons() {
	if (komicReserved.komicData.options.zoom === 'ON') {
		$('div.zoomIn, div.zoomOut').show();
	} else {
		$('div.zoomIn, div.zoomOut').hide();
	}
}
function setKomicSaveButton() {
	if (komicReserved.komicData.options.komicsave === 'OFF') {
		$('div.saveBookmark').hide();
		$('div.menuOn').css({ width: '100%' });
	}
}
function error(errorCode) {
	var mycomics = fillComicArray();

	switch(errorCode) {
		// Error while going next page
		case 1:
			// If on last comic
			if (parseInt(komicReserved.komicData.liveData.nextFolder) >= numrighe) {
				if (komicReserved.komicData.options.autojump === 'ON') {
					jAlert(true, KomicResources.strings.readingJS.nomorecomics, '');
					komicReserved.komicData.liveData.lastread = mycomics[parseInt(komicReserved.komicData.liveData.nextFolder) - 1].path;			
					// Return home not saving bookmark
					var timer = setTimeout(function() { 
						returnHome(false); 
					}, 2000);
				} else {
					jAlert(false, KomicResources.strings.readingJS.lastpage, '');
				}
			} else {
				if (komicReserved.komicData.options.autojump === 'ON') {
					// If next comic is not volume, set it. Else set the next one
					if (mycomics[parseInt(komicReserved.komicData.liveData.nextFolder)].path) {
						if (mycomics[parseInt(komicReserved.komicData.liveData.nextFolder)].volumeflag == true) {
							if (mycomics[parseInt(komicReserved.komicData.liveData.nextFolder) + 1].path) {
								komicReserved.komicData.liveData.lastread = mycomics[parseInt(komicReserved.komicData.liveData.nextFolder) + 1].path;
								komicReserved.komicData.liveData.nextFolder = parseInt(komicReserved.komicData.liveData.nextFolder) + 2;
								saveKomicReserved();
								KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicgetarray');
							}
						} else {
							komicReserved.komicData.liveData.lastread = mycomics[parseInt(komicReserved.komicData.liveData.nextFolder)].path;
							komicReserved.komicData.liveData.nextFolder = parseInt(komicReserved.komicData.liveData.nextFolder) + 1;
							saveKomicReserved();
							KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicgetarray');
						}
					}
				} else {
					jAlert(false, KomicResources.strings.readingJS.lastpage, '');
				}			
			}
		break;

		// Error while going previous page
		case 2:
			if (komicReserved.komicData.options.autojump === 'OFF') {
				jAlert(false, KomicResources.strings.readingJS.firstpage, '');
				komicReserved.komicData.liveData.pagenum = 0;
			} else {
				if (parseInt(komicReserved.komicData.liveData.nextFolder) - 2 < 0) {
					jAlert(false, KomicResources.strings.readingJS.firstcomic, '');
				} else if (mycomics[parseInt(komicReserved.komicData.liveData.nextFolder) - 2].path) {	
					if (mycomics[parseInt(komicReserved.komicData.liveData.nextFolder)-2].volumeflag == false) {
						komicReserved.komicData.liveData.lastread = mycomics[parseInt(komicReserved.komicData.liveData.nextFolder) - 2].path;
						komicReserved.komicData.liveData.nextFolder = parseInt(komicReserved.komicData.liveData.nextFolder) - 1;
						saveKomicReserved();
						KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicgetarray');
					} else {
						if (mycomics[parseInt(komicReserved.komicData.liveData.nextFolder) - 3].path) {
							komicReserved.komicData.liveData.lastread = mycomics[parseInt(komicReserved.komicData.liveData.nextFolder) - 3].path;
							komicReserved.komicData.liveData.nextFolder = parseInt(komicReserved.komicData.liveData.nextFolder) - 2;
							saveKomicReserved();
							KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicgetarray');
						}
					}
				}
			}
		break;
	}
}
function menuOn() {
	sessionStorage.setItem('stopautonext', true);
	setExtendedInfo(checkDeletion(komicReserved.komicData.liveData.lastread));
	$('div.slidebarBox, div.pixasticBox, div.readingButtons').hide();
	$('div.contextMenu, div.menuOff').show();
}
function menuOff() {
	setExtendedInfo(false);
	$('div.menuOff, div.contextMenu').hide();
	$('div.readingButtons').show();
}
function nextImage() {
	$boxToHide.hide();

	if (parseInt(komicReserved.komicData.liveData.pagenum) + 1 > (arrayPageName.length - 1)) {
		error(1);	
		return;
	}
	sessionStorage.setItem('pagesgone', parseInt(sessionStorage.getItem('pagesgone')) + 1);

	handleCanvas('remove');
	
	komicReserved.komicData.liveData.pagenum = parseInt(komicReserved.komicData.liveData.pagenum) + 1;
	
	$imageWrapper.empty().html('<img id="image" class="image" src="/mnt/us/documents/Komic/' + escape(komicReserved.komicData.liveData.lastread) + '/' + arrayPageName[parseInt(komicReserved.komicData.liveData.pagenum)] + '"/>');
	if (komicReserved.komicData.options.prealodImages === 'ON') {
		setActiveImage('image');
	} else {
		$imageWrapper.find('img.image').load(function(){setActiveImage('image')});
	}
	
	var sizeChanged = $imageWrapper.data('sizechanged'),
		zoomSize = $imageWrapper.data('zoomsize'),
		sizeLocked = $imageWrapper.data('sizelocked'),
		imageZoommed = $imageWrapper.data('imagezoommed');
	// If size changed but not locked ==> reset dimension (and zoom)
	// If size changed and locked ==> set scroll
	// Else, scroll to top	
	if (sizeChanged && sizeLocked) {
		var $thisImg = $('img.image');
		$thisImg.offset({
			top: $imageWrapper.data('hscroll'),
			left: $imageWrapper.data('vscroll')
		});
		$thisImg.css({
			zoom: parseInt(zoomSize)/100
		});
	}
	if (sizeChanged && !sizeLocked) {
		resetDimensions();
	// If size not changed but zoommed ==> reset zoom
	} else if (imageZoommed) {
		zoomInOut(null);
	}
	preLoadNext();
	setExtendedInfo(false, true);
	externalfunctions('both');
}
function prevImage() {
	$boxToHide.hide();
	if (parseInt(komicReserved.komicData.liveData.pagenum) == 0) {
		error(2);	
		return;
	}
	sessionStorage.setItem('pagesgone', parseInt(sessionStorage.getItem('pagesgone')) + 1);
	
	handleCanvas('remove');
	
	komicReserved.komicData.liveData.pagenum = parseInt(komicReserved.komicData.liveData.pagenum) - 1;	
	
	$imageWrapper.empty().html('<img id="image" class="image" src="/mnt/us/documents/Komic/' + escape(komicReserved.komicData.liveData.lastread) + '/' + arrayPageName[parseInt(komicReserved.komicData.liveData.pagenum)] + '"/>');
	if (komicReserved.komicData.options.prealodImages === 'ON') {
		setActiveImage('image');
	} else {
		$imageWrapper.find('img.image').load(function(){setActiveImage('image')});
	}
	
	var sizeChanged = $imageWrapper.data('sizechanged'),
		zoomSize = $imageWrapper.data('zoomsize'),
		sizeLocked = $imageWrapper.data('sizelocked'),
		imageZoommed = $imageWrapper.data('imagezoommed');
	// If size changed but not locked ==> reset dimension (and zoom)
	// If size changed and locked ==> set scroll
	// Else, scroll to top
	if (sizeChanged && sizeLocked) {
		var $thisImg = $('img.image');
		$thisImg.offset({
			top: $imageWrapper.data('hscroll'),
			left: $imageWrapper.data('vscroll')
		});
		$thisImg.css({
			zoom: parseInt(zoomSize)/100
		});
	}
	if (sizeChanged && !sizeLocked) {
		resetDimensions();
	// If size not changed but zoommed ==> reset zoom
	} else if (imageZoommed) {
		zoomInOut(null);
	}
	setExtendedInfo(false, true);
	externalfunctions('both');	
}
function promptSaveBookmark() {
	setPromptAlertOffset('set');
	jPrompt(false, KomicResources.strings.readingJS.savebmrkpromptmessage, '', KomicResources.strings.readingJS.savebmrkprompttitle, function(description) {
		if (description != null) {
			setPromptAlertOffset('reset');
			if (description === '') {
				description = KomicResources.strings.readingJS.nodescription;
			}
			saveBookmark(false, description);				
		}
	});
}
function saveBookmark(autosave, description) {
	var jsonBookmarks;	
	var bookmarkPresent = checkBookmarkedPage();		
	if (!bookmarkPresent) {	
		if (localStorage.getItem(komicReserved.komicData.liveData.lastread) == null) {
			jsonBookmarks = [];
		} else {
			jsonBookmarks = manageJSON('parse', localStorage.getItem(komicReserved.komicData.liveData.lastread));
		}
		jsonBookmarks.push({'page' : komicReserved.komicData.liveData.pagenum, 'description' : description});		
		localStorage.setItem(komicReserved.komicData.liveData.lastread, manageJSON('stringify', jsonBookmarks));
		if (autosave != true) {
			jAlert(true, KomicResources.strings.readingJS.bmrksaved, '');
			var timer = setTimeout(function() { $.alerts._hide(); }, 2000);
		}			
	} else {		
		if (autosave != true) {
			jAlert(false, KomicResources.strings.readingJS.alreadybmrk, '');
		}		
	}			
}
function checkBookmarkedPage() {
	var bookmarkPresent = false;	
	if (localStorage.getItem(komicReserved.komicData.liveData.lastread) != null) {
		var jsonBookmarks = manageJSON('parse', localStorage.getItem(komicReserved.komicData.liveData.lastread));		
		// We check if the page exists
		for (var i=0; i<jsonBookmarks.length; i++) {
			if (parseInt(jsonBookmarks[i].page) === parseInt(komicReserved.komicData.liveData.pagenum)) {
				bookmarkPresent = true;				
			}
		}
	}	
	return bookmarkPresent;	
}
function removeThisBookmark() {
	if (localStorage.getItem(komicReserved.komicData.liveData.lastread) == null) {	
		jAlert(false, KomicResources.strings.readingJS.nobmrkfolder, '');				
	} else {		
		var jsonBookmarks = manageJSON('parse', localStorage.getItem(komicReserved.komicData.liveData.lastread));
		var bookmarkPresent = checkBookmarkedPage();	

		// If the page isn't bookmarked, we present a different altert. Else, we check the length of the array and remove it if necessary
		if (!bookmarkPresent) {
			jAlert(false, KomicResources.strings.readingJS.nobmrkpage, '');
		} else {
			// We check if the page exists and, if so, we remove it and we present and alert
			for (var i=0; i<jsonBookmarks.length; i++) {
				if (parseInt(jsonBookmarks[i].page) == parseInt(komicReserved.komicData.liveData.pagenum)) {
					jsonBookmarks.splice( i, 1 );			
					jAlert(false, KomicResources.strings.readingJS.bmrkremoved, '');
				}
			}
			if (jsonBookmarks.length == 0) {			
				localStorage.removeItem(komicReserved.komicData.liveData.lastread);				
			} else {
				localStorage.setItem(komicReserved.komicData.liveData.lastread, manageJSON('stringify', jsonBookmarks));
			}
		}
	}

}
function promptJumpToPage() {
	var message = KomicResources.strings.readingJS.currentpage + komicReserved.komicData.liveData.pagenum + '<br/>' +  KomicResources.strings.readingJS.jumppromptmessage; 	
	var maxpage = (arrayPageName.length - 1);
	var range = '0-' + maxpage;
	setPromptAlertOffset('set');
	
	jPrompt(true, message, range, KomicResources.strings.readingJS.jumpprompttitle, function(pageNumber) {
		if ((isNaN(pageNumber)) == true || pageNumber === '' || pageNumber === ' ') {
			jAlert(false, KomicResources.strings.readingJS.choosepage, '');		
		} else if (pageNumber == null) {							
		} else if (parseInt(pageNumber) > (arrayPageName.length - 1)) {
			jAlert(false, KomicResources.strings.readingJS.nonexistingpage, '');
		} else if (pageNumber != null && pageNumber != '') {			
			jumpToImage (pageNumber, false);				
		}
		setPromptAlertOffset('reset');
	});
}
function jumpToImage(pageNumber, jumpedFromBookmark) {
	sessionStorage.setItem('pagesgone', parseInt(sessionStorage.getItem('pagesgone')) + 1);
	if (jumpedFromBookmark) {
		$('div.readingButtons').show();
	}
	
	handleCanvas('remove');
	
	komicReserved.komicData.liveData.pagenum = pageNumber;
	
	$imageWrapper.empty().html('<img id="image" class="image" src="/mnt/us/documents/Komic/' + escape(komicReserved.komicData.liveData.lastread) + '/' + arrayPageName[parseInt(komicReserved.komicData.liveData.pagenum)] + '"/>');
	if (komicReserved.komicData.options.prealodImages === 'ON') {
		setActiveImage('image');
	} else {
		$imageWrapper.find('img.image').load(function(){setActiveImage('image')});
	}
	
	var sizeChanged = $imageWrapper.data('sizechanged'),
		zoomSize = $imageWrapper.data('zoomsize'),
		sizeLocked = $imageWrapper.data('sizelocked'),
		imageZoommed = $imageWrapper.data('imagezoommed');
	// If size changed but not locked ==> reset dimension (and zoom)
	// If size changed and locked ==> set scroll
	// Else, scroll to top
	if (sizeChanged && sizeLocked) {
		var $thisImg = $('img.image');
		$thisImg.offset({
			top: $imageWrapper.data('hscroll'),
			left: $imageWrapper.data('vscroll')
		});
		$thisImg.css({
			zoom: parseInt(zoomSize)/100
		});
	}
	if (sizeChanged && !sizeLocked) {
		resetDimensions();
	// If size not changed but zoommed ==> reset zoom
	} else if (imageZoommed) {
		zoomInOut(null);
	}
	setExtendedInfo(false, true);
	externalfunctions('both');
}
function scrollDownPage() {
	var $image = $('img.image'),
		topPosition = $image.position().top,
		windowHeight = $(window).height(),
		imageHeight = $image.height(),
		scrollHeight = parseInt(komicReserved.komicData.options.scrollheight);
	
	if ((imageHeight - windowHeight - scrollHeight - Math.abs(topPosition)) < 0) {
		$image.css({
			top: -(imageHeight - windowHeight)
		});
	} else {
		$image.css({
			top: (topPosition -= scrollHeight)
		});
	}
}
function fillBookmarkPopup() {
	var mybookmarks = [];

	if (!localStorage.getItem(komicReserved.komicData.liveData.lastread)) {
		mybookmarks.push({header: '', page: KomicResources.strings.shared.nobookmarksavailable, description: ''});		
	} else {
		var jsonBookmarks = manageJSON('parse', localStorage.getItem(komicReserved.komicData.liveData.lastread));
		jsonBookmarks = sortJSON(jsonBookmarks, 'page');
		$.each(jsonBookmarks, function(i, v) {
			mybookmarks.push({header: KomicResources.strings.shared.page, page: this.page, description: this.description});
		});
	}
	return mybookmarks;
}
function preLoadNext() {
	if (komicReserved.komicData.options.prealodImages === 'ON') {
		newImg.src = '/mnt/us/documents/Komic/' + escape(komicReserved.komicData.liveData.lastread) + '/' + arrayPageName[ parseInt(komicReserved.komicData.liveData.pagenum) + 1 ];
		newImg.onload = function () {
			preloadedDimensions[arrayPageName[parseInt(komicReserved.komicData.liveData.pagenum) + 1]] = {width: newImg.naturalWidth, height: newImg.naturalHeight};
		};
	}
}
function promptResize() {
	setPromptAlertOffset('set');

	jPrompt(false, KomicResources.strings.readingJS.sizepromptmessage, '', KomicResources.strings.readingJS.sizeprompttitle, function(dimension) {
		if ((isNaN(dimension)) == true || dimension === '' || dimension === ' ') {				
			jAlert(false, KomicResources.strings.readingJS.choosesize, '');	
		} else if (dimension == null) {				
		} else if (dimension != null && dimension != '') {			
			resizeImg(dimension);				
		}
		setPromptAlertOffset('reset');
	});
}
function resizeImg(resize) {
	handleCanvas('remove');
	
	// Hide zoom button as long as the size is altered and reset zoom level
	$('div.zoomIn, div.zoomOut').hide();
	zoomInOut(null);

	$('img.image').css('zoom', parseInt(resize)/100);		
	if (resize == 100) $imageWrapper.removeData('sizechanged');
	else {
		$imageWrapper.data('sizechanged', true);
		$imageWrapper.data('zoomsize', resize);
	}
}
function zoomInOut(button) {
	handleCanvas('remove');
	switch(button) {
		case 3:
			zoom += 0.2;
		break;
		case 4:
			zoom += -0.2;
		break;
		default:
			zoom = 1;
	}
	if (zoom < 0.2) zoom = 0.2;
	if (zoom != 1) $imageWrapper.data('imagezoommed', true);
	else $imageWrapper.removeData('imagezoommed');
	
	$('img.image').css('zoom', zoom);
}
function lockSize() {
	$imageWrapper.data('sizelocked', true);
	$imageWrapper.data('hscroll', $('img.image').offset().left);
	$imageWrapper.data('vscroll', $('img.image').offset().top);
	jAlert(false, KomicResources.strings.readingJS.locked, '');
}
function unLockSize(noAlert) {
	$imageWrapper.removeData('sizelocked');
	$imageWrapper.removeData('hscroll');
	$imageWrapper.removeData('vscroll');
	if (noAlert != true) {
		jAlert(false, KomicResources.strings.readingJS.unlocked, '');
	}
}
function resetDimensions() {
	// If zoommed, reset zoom
	var imageZoommed = $imageWrapper.data('imagezoommed');
	if (imageZoommed) {
		zoomInOut(null);
	}
	// Since we hide zoom button when changing size, show them
	setKomicZoomButtons();
	
	$imageWrapper.removeData('sizechanged');
	$imageWrapper.removeData('sizelocked');

	$('img.image').css('zoom', 1);
	if (komicReserved.komicData.options.imageratio === 'ON') {
		setActiveImage('internalCall');
	} else {
		$('img.image').css({
			top: 0, 
			left: 0
		});
	}
}
function screenshot() {	
	var timer = setTimeout(function() { 
		KomicKindleHub.messaging.sendMessage('com.lab126.system', 'takeScreenShot', '1'); 
	}, 50);
	timer = setTimeout(function() { 
		jAlert(false, KomicResources.strings.readingJS.screenshot, ''); 
	}, 200);
}
function toggleDeletion() {
	var jsonDelete;
	var deletePresent = checkDeletion(komicReserved.komicData.liveData.lastread);
	if (deletePresent) {	
		jsonDelete = manageJSON('parse', localStorage.getItem('todelete'));
		for (var i = 0; i < jsonDelete.length; i++ ) {				
			if (jsonDelete[i].folder === komicReserved.komicData.liveData.lastread) {					
				jsonDelete.splice( i, 1 );
			} 					
		}		
		if (jsonDelete.length == 0) {
			localStorage.removeItem('todelete');
		} else {
			localStorage.setItem('todelete', manageJSON('stringify', jsonDelete));				
		}	
		jAlert(false, KomicResources.strings.readingJS.deletionremoved, '');		
	} else {
		var lastId;
		if (localStorage.getItem('todelete') == null) {
			jsonDelete = [];
			lastId = 0;
		} else {
			jsonDelete = manageJSON('parse', localStorage.getItem('todelete'));
			jsonDelete = sortJSON(jsonDelete, 'id');
			lastId = parseInt(jsonDelete[jsonDelete.length-1].id) + 1;
		}		
		jsonDelete.push({'id' : lastId, 'folder' : komicReserved.komicData.liveData.lastread});		
		localStorage.setItem('todelete', manageJSON('stringify', jsonDelete));
		jAlert(false, KomicResources.strings.readingJS.deletionscheduled, '');	
	}
}
function checkDeletion(comicToDelete) {
	var deletePresent = false;	
	if (localStorage.getItem('todelete')) {
		var jsonDelete = manageJSON('parse', localStorage.getItem('todelete'));
		// We check if the comic exists	
		$.each(jsonDelete, function(i, v) {
			if (jsonDelete[i].folder == comicToDelete) {
				deletePresent = true;				
			}
		});
	}	
	return deletePresent;	
}
function setActiveImage (imagetype) {
	switch(imagetype) {	
		case 'internalCall':
			computeRatio(false);
			break;		
		case 'image':
			// If imageratio is ON and size not locked, compute ratio
			if (komicReserved.komicData.options.imageratio === 'ON') {
				computeRatio(false);
			}
			// Se � in corso un cambio effetto o devo riprisinare tutto
			if ($imageWrapper.data('imagealtered') && $imageWrapper.data('tobeprocessed')) {
				handleCanvas('replace');
			}
			break;			
	}	
}
function computeRatio(loaded) {
	var image_height,
		image_width,
		window_width = $(window).width(),
		window_height = $(window).height();
		
	if (preloadedDimensions[arrayPageName[parseInt(komicReserved.komicData.liveData.pagenum)]] != null) {
		image_height = preloadedDimensions[arrayPageName[parseInt(komicReserved.komicData.liveData.pagenum)]].height;
		image_width = preloadedDimensions[arrayPageName[parseInt(komicReserved.komicData.liveData.pagenum)]].width;
	} else if (loaded) {	
		var imageDimensions = getNaturalDimensions($('img.image'));
		image_height = imageDimensions.height;
		image_width = imageDimensions.width;
	} else {
		$imageWrapper.find('img.image').load(function(){computeRatio(true)});
		return;
	}
	
	var height_ratio = image_height / window_height;
	var width_ratio = image_width / window_width;
	
	$('img.image').css({
		top: 0, 
		left: 0
	});
	
	// Set width, height and scroll position (if portrait)
	if (komicReserved.komicData.liveData.orientation === 'landscape') {
		var altezza = window_width*(image_height/image_width);
		$('img.image').width(window_width);
		$('img.image').height(altezza);		
	} else {	
		if (height_ratio > width_ratio) {		
			var larghezza = window_height/(image_height/image_width);
			var scrollx = (window_width-larghezza)/2;
			$('img.image').css({
				width: larghezza, 
				height: window_height,
				left: scrollx
			});
		} else {		
			var altezza = window_width*(image_height/image_width);
			var scrolly = (window_height-altezza)/2;
			$('img.image').css({
				width: window_width, 
				height: altezza,
				top: scrolly
			});
		}
	}	
}		
function getNaturalDimensions($mainImage) {
    var mainImage = $mainImage[0],
        dimensions = {};
	dimensions.width = mainImage.naturalWidth;
	dimensions.height = mainImage.naturalHeight;
    return dimensions;
}
function singleClick(button) {
	switch(button) {
		case 0:
			prevImage();
		break;
		case 1:
			nextImage();
		break;
		case 2:
			menuOn();
		break;
		case 3:
			zoomInOut(button);
		break;
		case 4:
			zoomInOut(button);
		break;
		case 5:
			saveBookmark(false, KomicResources.strings.optionsHTML.saveButtonOption);
		break;
		case 6:
			scrollDownPage();
		break;
	}
}
function autoPage(pageInterval, pageCounter) {
   	var pageTimeout = setTimeout(function () {
      	if (sessionStorage.getItem('stopautonext') === 'false') {
			nextImage();
		} else {
			clearTimeout(pageTimeout);
		}
		pageCounter++;
		if (pageCounter < (arrayPageName.length - 1) && sessionStorage.getItem('stopautonext') === 'false') {
			autoPage(pageInterval, pageCounter);
		}
   	}, pageInterval)
}
function autoScroll(scrollInterval, numScroll, pageCounter, scrollCounter) {
   	var scrollTimeout = setTimeout(function () {
		if (sessionStorage.getItem('stopautonext') === 'false' && numScroll != 0) {
			scrollDownPage();
		} else if (sessionStorage.getItem('stopautonext') === 'true') {
			clearTimeout(scrollTimeout);
		}
		scrollCounter++;
		if (scrollCounter <= numScroll && sessionStorage.getItem('stopautonext') === 'false') {
			autoScroll(scrollInterval, numScroll, pageCounter, scrollCounter);
		} else if (scrollCounter > numScroll && sessionStorage.getItem('stopautonext') === 'false' && pageCounter < (arrayPageName.length - 1)) {
			nextImage();
			var newNumscrolls = countScrolls();
			pageCounter++;
			autoScroll(scrollInterval, newNumscrolls, pageCounter, 0);
		}
   	}, scrollInterval)
}
function autoNextPage(pageInterval) {
	handleCanvas('revert');
	sessionStorage.setItem('stopautonext', false);	
	var pageCounter = parseInt(komicReserved.komicData.liveData.pagenum);
	autoPage(parseInt(pageInterval) * 1000, pageCounter);
}
function autoNextScroll(scrollInterval) {
	handleCanvas('revert');
	sessionStorage.setItem('stopautonext', false);
	var pageCounter = parseInt(komicReserved.komicData.liveData.pagenum);	
	var numScroll = countScrolls();
	autoScroll(parseInt(scrollInterval) * 1000, numScroll, pageCounter, 0);
}
function countScrolls() {
	var numScroll = Math.round(($('img.image').height() - 500) / parseInt(komicReserved.komicData.options.scrollheight) );
	return numScroll;
}
function showValuePostprocessing(whichEffect, plusMinus) {
	switch (whichEffect) {
		case 'contrast':
			var brightnesscontrast = getAndSetValues('get', null);
			switch (plusMinus) {
				case 'minus':
					var newValueContrast = brightnesscontrast[1] - 2;
				break;
				case 'plus':
					var newValueContrast = brightnesscontrast[1] + 2;
				break;
			}
			if (newValueContrast > 30) {
				newValueContrast = 30;
			} else if (newValueContrast < -10) {
				newValueContrast = -10;
			}
			brightnesscontrast[1] = newValueContrast;
			getAndSetValues('set', brightnesscontrast);
			$('span.contrastValueNumber').text(newValueContrast/10);
		break;
		case 'brightness':
			var brightnesscontrast = getAndSetValues('get', null);
			switch (plusMinus) {
				case 'minus':
					var newValueBrightness = brightnesscontrast[0] - 10;
				break;
				case 'plus':
					var newValueBrightness = brightnesscontrast[0] + 10;
				break;
			}
			if (newValueBrightness > 150) {
				newValueBrightness = 150;
			} else if (newValueBrightness < -150) {
				newValueBrightness = -150;
			}	
			brightnesscontrast[0] = newValueBrightness;
			getAndSetValues('set', brightnesscontrast);
			$('span.brightnessValueNumber').text(newValueBrightness);
		break;
	}
}
function getAndSetValues(action, brightnesscontrast) {
	switch (action) {
		case 'set':
			var values = brightnesscontrast[0] + '/' + brightnesscontrast[1];
			komicReserved.komicData.options.brightnesscontrast = values; 
		break;
		case 'get':
			var values = (komicReserved.komicData.options.brightnesscontrast).split('/');
			if (values.length != 2) {
				komicReserved.komicData.options.brightnesscontrast = '0/0';
				values = (komicReserved.komicData.options.brightnesscontrast).split('/');
			}
			var mybrightness = parseInt(values[0]);
			var mycontrast = parseInt(values[1]);
			return [mybrightness, mycontrast];
		break;
	}
}
function setContrastBrightness(revertBefore) {	
	var brightnesscontrast = getAndSetValues('get', null);
	var mycontrast = brightnesscontrast[1]/10;
	// Revert before applying new values
	if (revertBefore) Pixastic.revert(document.getElementById('image'));
	// Apply new values
	$('img.image').pixastic('brightness', {brightness:brightnesscontrast[0],contrast:mycontrast});
	$imageWrapper.data('imagealtered', true);
}
function handleCanvas(action) {
	var imagealtered = $imageWrapper.data('imagealtered');
	var tobeprocessed = $imageWrapper.data('tobeprocessed');
	switch(action) {
		case 'revert':
			// Revert pixastic effect but not reset contrast brightness values
			// This is triggered by reading menu
			if (imagealtered) {
				Pixastic.revert(document.getElementById('image'));
				$imageWrapper.removeData('imagealtered');
			}
		break;
		case 'replace':
			// Sets pixastic back again and reset any flag
  			if (imagealtered && tobeprocessed) {
				setContrastBrightness(false);
				$imageWrapper.removeData('tobeprocessed');
			}			
		break;
		case 'remove':
			// Temporarly removes pixastic and sets everything to process image later 
			if (imagealtered) {
				Pixastic.revert(document.getElementById('image'));
				$imageWrapper.data('tobeprocessed', true);			
			}
		break;
		case 'reset':
			// Reset pixastic effects by reverting image element and setting brightness and contrast values
			// This is triggered by pixastic box
			Pixastic.revert(document.getElementById('image'));
			komicReserved.komicData.options.brightnesscontrast = '0/0';
			$('span.brightnessValueNumber').text(0);
			$('span.contrastValueNumber').text(0);
			$imageWrapper.removeData('imagealtered');
			$imageWrapper.removeData('tobeprocessed');
		break;
	}
}
function externalfunctions(action) {
	switch(action) {
		case 'both':
			var currentrate = parseInt(komicReserved.komicData.options.refreshrate);
			if (parseInt(sessionStorage.getItem('pagesgone')) % currentrate == 0) {
				var timer = setTimeout(function() { KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicrefreshscreen'); }, 2000);
			}
			if (parseInt(sessionStorage.getItem('pagesgone')) % 10 == 0) {
				KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicfreememory');
			}
		break;
		case 'memory':
			KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicfreememory');
			KomicKindleHub.dev.clearCache();
			KomicKindleHub.dev.clearApplicationCache();
		break;
		case 'refresh':
			KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicrefreshscreen');
		break;
	}
}
function mouseUpCallback(button) {
	flagSwipe = false;
	mouseYinitpos = null;
	mouseXinitpos = null;
	offsetYimage = null;
	offsetXimage = null;
	start = null;

	if (flagSwiped == false) {
		singleClick(button);
	} else {	
		// I wait a little bit to let the screen settle then I refresh the screen and reset the flag
		var timer = setTimeout(function() { 
			externalfunctions('refresh'); 
		}, 1000);	
		flagSwiped = false;		
	}
}
function trackMouse() {
	// This is executed only when you start swiping and not during the swipe
	if (flagSwipe == false) {	
		mouseYinitpos = parseInt(window.event.clientY);
		mouseXinitpos = parseInt(window.event.clientX);
		offsetYimage = $('img.image').offset().top;
		offsetXimage = $('img.image').offset().left;
		flagSwipe = true;
		start = (new Date()).getTime();	
	}
	
	var mouseYcurrpos = parseInt(window.event.clientY);
	var mouseXcurrpos = parseInt(window.event.clientX);
	var deltaYPos = mouseYcurrpos - mouseYinitpos;
	var deltaXPos = mouseXcurrpos - mouseXinitpos;
	var totalYmov = offsetYimage + deltaYPos;
	var totalXmov = offsetXimage + deltaXPos;
	
	var end = (new Date()).getTime();
	var elapsedTime = (end - start);
	
	if (elapsedTime > parseInt(sessionStorage.getItem('elapsedTimeSet')) && (Math.abs(deltaYPos) > parseInt(sessionStorage.getItem('deltaYPosSet')) || Math.abs(deltaXPos) > parseInt(sessionStorage.getItem('deltaXPosSet')))) {
		$('img.image').css({
			top: totalYmov, 
			left: totalXmov
		});
		flagSwiped = true;		
	}
}
function getCalibrationData() {
	if (!komicReserved.komicData.options.calibration) {
		komicReserved.komicData.options.calibration = '10/100/100';
	}
	// I add a tolerance of 10%. This means you have to swipe for the 10% of time/space more. Just in case you were too accurate during the calibration
	var values = (komicReserved.komicData.options.calibration).split('/');
	sessionStorage.setItem('elapsedTimeSet', (parseInt(values[0]) + (parseInt(values[0])* 10)/100));
	sessionStorage.setItem('deltaXPosSet', (parseInt(values[1]) + (parseInt(values[1])* 10)/100));
	sessionStorage.setItem('deltaYPosSet', (parseInt(values[2]) + (parseInt(values[2])* 10)/100));
}
function setExtendedInfo(comicGoesToBin, directTitleBar) {
	if ((directTitleBar && komicReserved.komicData.options.komictitlebar === 'ON') || komicReserved.komicData.options.komictitlebar === 'ON') {
		var titlebar = updateTitleBar(comicGoesToBin);
		KomicKindleHub.chrome.setTitleBar(titlebar[0], titlebar[1]);
	} else if(!directTitleBar && komicReserved.komicData.options.komictitlebar === 'OFF') {
		if ($('div.extendedInfoBox').length > 0) {
			$('div.extendedInfoBox').remove();
		} else {
			$('body').append('<div class="extendedInfoBox"></div>');
			setDate($('div.extendedInfoBox'));
			setInfo($('div.extendedInfoBox'), comicGoesToBin);
		}
	}
}
function updateTitleBar(comicGoesToBin) {
	var titlebar = [];
	if (comicGoesToBin) {
		titlebar[0] = '';
		titlebar[1] = KomicResources.strings.readingJS.titlebardeletion;
	} else {
		var numberOfPage = (arrayPageName.length - 1);
		titlebar[0] = KomicResources.strings.shared.page + komicReserved.komicData.liveData.pagenum + '/' + numberOfPage;
		titlebar[1] = komicReserved.komicData.liveData.lastread;
	}
	return titlebar;
}
function setDate(extendedInfoBox) {
	var newDate = new Date(),
		$extendedInfoBox = $(extendedInfoBox);
	$extendedInfoBox.append('<span style="font-size: xx-large;">' + addLeadingZero(newDate.getHours()) + ':' + addLeadingZero(newDate.getMinutes()) + '</span>');
}
function setInfo(extendedInfoBox, comicGoesToBin) {
	var $extendedInfoBox = $(extendedInfoBox),
		titlebar = updateTitleBar(comicGoesToBin);
	$extendedInfoBox.append('<span style="font-size: large;">' + titlebar[1] + '</span><span style="font-size: large;">' + titlebar[0] + '</span>');
}
function addLeadingZero(i) {
	if (i<10) {
		i='0' + i;
	}
	return i;
}