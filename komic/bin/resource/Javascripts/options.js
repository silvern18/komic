// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

var storedData = [];

function onLoad() {
	checkJSON('options');
	
	komicReserved = manageJSON('parse', localStorage.getItem('komicReserved'));
	KomicKindleHub.chrome.setTitleBar(KomicResources.strings.optionJS.optionsTitle0, 'Komic');

	setKomicHandlers();
	appendText();
	setupToggleButtons();
	setOptionCSS();
}
function setOptionCSS() {
	if (komicReserved.komicData.liveData.orientation === 'landscape') {
		if (komicReserved.komicData.options.komictitlebar === 'OFF') {
			$('div.optionsContainer ul li').css({ 
				paddingTop: '20px' 
			});
		} else {
			$('div.optionsContainer ul li').css({ 
				paddingTop: '15px' 
			});
		}
	}	
}
function setKomicHandlers() {
	$('button.jqueryButtons').button();

	$('img.leftArrowButton, img.rightArrowButton').click(function () {
		changeOptionScreen($(this).attr('class'));
	});
	$('button.exitKomicButton').click(function () {
		KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicExitNow');
	});
	$('button.returnHomeButton').click(function () {
		returnHome();
	});
	$('div.calibrationBox').mousedown(function () {
		mouseDown();
	}).mouseup(function () {
		mouseUp();
	});
	$('div.optionsContainer img:not(.notDefaultButton)').click(function () {
		toggleOption($(this));
	});
	$('div.optionsContainer img.refreshRateButton').click(function () {
		new KomicRefreshMenu().showRefreshMenu();
	});
	$('div.optionsContainer img.backgroundColorButton').click(function () {
		new KomicColorMenu().showColorMenu();
	});
	$('div.optionsContainer img.calibrationButton').click(function () {
		startStopCalibration('start');
	});
}
function appendText() {
	$('button.exitKomicButton').text(KomicResources.strings.buttons.exit);
	$('button.returnHomeButton').text(KomicResources.strings.buttons.returnhome);
	$('span.pageHeader').text(KomicResources.strings.version.ver);
	$('span.smallPageHeader').text(KomicResources.strings.shared.optionsmenu);
	$('div.optionsContainer ul span').each(function () {
		var thisClass = $(this).attr('class').replace('optionTags ', '');
		$(this).text(KomicResources.strings.optionsHTML[thisClass]).click(function () {
			jAlert(false, KomicResources.strings.optionJS[thisClass], '');
		});
	});
}
function setupToggleButtons() {
	var optionValue,
		optionName;
		
	$('div.optionsContainer img:not(.notDefaultButton)').each(function () {
		optionName = $(this).data('optionname');
		optionValue = komicReserved.komicData.options[optionName];
		$(this).attr('src', 'resource/Images/' + optionValue + '.png').data('optionvalue', komicReserved.komicData.options[optionName]);
	});
}
function toggleOption(button) {
	var $button = $(button),
		currentValue = $button.data('optionvalue'),
		optionName = $button.data('optionname'),
		wantedValue,
		imgSrc;
		
	if (optionName === 'landscape') {
		wantedValue = currentValue === 'landscapeRight' ? 'landscapeLeft' : 'landscapeRight';
	} else {
		wantedValue = currentValue === 'ON' ? 'OFF' : 'ON';
	}
		
	$button.attr('src', 'resource/Images/' + wantedValue + '.png').data('optionvalue', wantedValue);
	komicReserved.komicData.options[optionName] = wantedValue;
	
	if (optionName === 'komicsounds') {
		KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komicsounds');
	} else if (optionName === 'komictitlebar') {
		if (wantedValue === 'ON') {
			KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komictitlebarON');
			var currentOptionScreen = $('div.optionsContainer ul.currentOptionScreen').index();
			var timer = setTimeout(function() { 
				KomicKindleHub.chrome.setTitleBar(KomicResources.strings.optionJS['optionsTitle' + currentOptionScreen], 'Komic');
			}, 1000);
		} else {
			KomicKindleHub.messaging.sendMessage('com.lab126.system', 'sendEvent', 'komictitlebarOFF');
		}
		setOptionCSS();
	}
}
function changeOptionScreen(buttonClass) {
	var $optionScreens = $('div.optionsContainer ul');
	var currentOptionScreen = $('div.optionsContainer ul.currentOptionScreen').index(); // TODO sistema con find da variabile sopra

	if (buttonClass === 'rightArrowButton') {
		currentOptionScreen++;
	} else {
		currentOptionScreen--;
	}
	
	$optionScreens.removeClass('currentOptionScreen');	
	$optionScreens.eq(currentOptionScreen).show().addClass('currentOptionScreen');
	$optionScreens.not('.currentOptionScreen').hide();
	
	KomicKindleHub.chrome.setTitleBar(KomicResources.strings.optionJS['optionsTitle' + currentOptionScreen], 'Komic');
	
	if (currentOptionScreen == 0) {
		$('img.leftArrowButton').hide();
		$('img.rightArrowButton').show();
	} else if (currentOptionScreen == 1 || currentOptionScreen == 2) {
		$('img.leftArrowButton').show();
		$('img.rightArrowButton').show();
	} else if (currentOptionScreen == 3) {
		$('img.leftArrowButton').show();
		$('img.rightArrowButton').hide();
	}
	
}
function startStopCalibration(action) {
	switch(action) {	
		case 'start':	// TODO use find
			$('table.calibrationTable thead, table.calibrationTable tbody, table.calibrationTable tfoot').empty();
			$('span.calibrationAreaTitle').empty().text(KomicResources.strings.optionsHTML.calibrationOptionText0);
			$('table.calibrationTable thead').append('<tr><th>X px</th><th>Y px</th><th>msec</th></tr>');		
			$('div.calibrationBox').show();
			$('<div class="calibrationBackLayer"></div>').click(function() {
				startStopCalibration('stop');
			}).appendTo($('div.container'));
			storedData = [];
			break;	
		case 'stop':
			$('div.calibrationBox').hide();
			$('div.calibrationBackLayer').remove();
			break;	
	}
}
function mouseDown() {	
	// Get first mouse position
	mouseYinitpos = parseInt(window.event.clientY);
	mouseXinitpos = parseInt(window.event.clientX);
	
	// Get mousedown time
	start = (new Date()).getTime();
}
function mouseUp() {
	if (storedData.length == 3) return;

	// Get last mouse position	
	var mouseYfinalpos = parseInt(window.event.clientY);
	var mouseXfinalpos = parseInt(window.event.clientX);
	
	// Update the deltapos	
	var deltaYpos = Math.abs(mouseYfinalpos - mouseYinitpos);
	var deltaXpos = Math.abs(mouseXfinalpos - mouseXinitpos);
	
	// Get the new date and the difference in milliseconds
	var end = (new Date()).getTime();
	var diff = end - start;
	
	// Push a new object with space and time data
	storedData.push({
		spaceX: deltaXpos,
		spaceY: deltaYpos,
		time: diff
	});
		
	$('span.calibrationAreaTitle').text(KomicResources.strings.optionsHTML['calibrationOptionText' + storedData.length]);
	$('table.calibrationTable tbody').append('<tr><td>' + storedData[storedData.length - 1].spaceX + '</td><td>' + storedData[storedData.length - 1].spaceY + '</td><td>' + storedData[storedData.length - 1].time + '</td></tr>');
	
	if (storedData.length == 3) {
		$('table.calibrationTable tfoot').append('<tr><td colspan="3">' + KomicResources.strings.optionsHTML.calibrationOptionAverage + '</td></tr>');
		setAverage();
	}
}
function setAverage() {
	var totalElapsedTime = 0,
		totalDeltaYPos = 0,
		totalDeltaXPos = 0;
	
	for (var i = 0; i < storedData.length; i++) {
		totalElapsedTime += storedData[i].time;
		totalDeltaYPos += storedData[i].spaceY;
		totalDeltaXPos += storedData[i].spaceX;
	}
	
	totalElapsedTime = Math.round( (totalElapsedTime / 3) * 10 ) / 10;
	totalDeltaXPos = Math.round( (totalDeltaXPos / 3) * 10 ) / 10;
	totalDeltaYPos = Math.round( (totalDeltaYPos / 3) * 10 ) / 10;	

	$('table.calibrationTable tfoot').append('<tr><td>' + totalDeltaXPos + '</td><td>' + totalDeltaYPos + '</td><td>' + totalElapsedTime + '</td></tr>');
	komicReserved.komicData.options.calibration = totalElapsedTime + '/' + totalDeltaXPos + '/' + totalDeltaYPos;
}