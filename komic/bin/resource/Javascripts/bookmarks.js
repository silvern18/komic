// Written by Silver18
// more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

function onLoad() {
	checkJSON('options');
	komicReserved = manageJSON('parse', localStorage.getItem('komicReserved'));
	KomicKindleHub.chrome.setTitleBar(KomicResources.strings.shared.managebookmarks, 'Komic');
	appendText();
	manageBookmark();
	setCSS();
	setKomicHandlers();
}
function setKomicHandlers() {
	$('button.jqueryButtons').button();
	
	$('button.removeBookmarkButton').click(function () {
		showBookmarkPopup();
	});
	$('button.clearBookmarksButton').click(function () {
		clearAllBookmark();
	});
	$('button.returnHomeButton').click(function () {
		returnHome();
	});
}
function setCSS() {
	if (komicReserved.komicData.liveData.orientation === 'portrait') {
		$('ul.availableBookmark').css({ 'max-height': '540px' });
	} else {
		$('ul.availableBookmark').css({ 'max-height': '340px' });
	}
}
function manageBookmark() {
	var keys = [],
		$availableBookmark = $('ul.availableBookmark');
	
	if ($availableBookmark.find('li').length > 0) {			
		$availableBookmark.empty();
	}
	for (var i=0; i<=localStorage.length-1; i++) {			
		keys[i] = localStorage.key(i);				
	}
	
	removeEntries(keys);
	keys.sort();

	if (keys.length == 0) {			
		$('<li>' + KomicResources.strings.shared.nobookmarksavailable + '</li>').appendTo($availableBookmark);			
	} else {
		for (var i=0; i<keys.length; i++) {
			if (keys[i] != null) {
				$('<li>' + keys[i] + '</li>').appendTo($availableBookmark);
			}
		}
	}
}
function appendText() {
	$('button.removeBookmarkButton').text(KomicResources.strings.buttons.removeabookmark);
	$('button.clearBookmarksButton').text(KomicResources.strings.buttons.removeall);
	$('button.returnHomeButton').text(KomicResources.strings.buttons.returnhome);
	$('span.pageHeader').text(KomicResources.strings.version.ver);
	$('span.smallPageHeader').text(KomicResources.strings.shared.managebookmarks);
	$('span.availableBookmarkHeader').text(KomicResources.strings.bookmarksJS.available);
}
function fillRemoveBookmark() {
	var obj,
		mybookmark = [],
		keys = [];

	for (var i=0; i<=localStorage.length-1; i++) {			
		keys[i] = localStorage.key(i);				
	}
	removeEntries(keys);
	keys.sort();
	if (keys.length == 0) {
		obj = {comic: KomicResources.strings.shared.nobookmarksavailable};
		mybookmark.push(obj);
	} else {	
		for (var i=0; i<=keys.length-1; i++) {				
			obj = {comic: keys[i]};
			mybookmark.push(obj);					
		}	
	}	
	return mybookmark;
}
function removeBookmark() {
	$('#bookmarkList li').each(function(index) {
		if ($(this).data('selected') == true) {
			localStorage.removeItem($(this).text());
		}
	});
	manageBookmark();
}
function clearAllBookmark() {
	var saveit1 = false,
		saveit2 = false,
		saveit3 = false;

	if (localStorage.getItem('todelete')) {
		var temporary1 = localStorage.getItem('todelete');
		saveit1 = true;
	}
	if (localStorage.getItem('removeborders')) {
		var temporary2 = localStorage.getItem('removeborders');
		saveit2 = true;
	}
	if (localStorage.getItem('komicReserved')) {
		var temporary3 = localStorage.getItem('komicReserved');
		saveit3 = true;
	}
	localStorage.clear();
	if (saveit1 == true) {
		localStorage.setItem('todelete', temporary1);
	}
	if (saveit2 == true) {
		localStorage.setItem('removeborders', temporary2);
	}
	if (saveit3 == true) {
		localStorage.setItem('komicReserved', temporary3);
	}
	manageBookmark();	
}
function removeEntries(array) {
	for(var i=0; i<array.length; i++) { 
		if (array[i] === 'todelete' || array[i] === 'removeborders' || array[i] === 'komicReserved') {
			array.splice(i,1);
		}
	}
}
function showBookmarkPopup() {
	var type = 'bookmark';
	hideMyBookmark();
	showMyBookmark();

	$('body').append(
		'<div id="bookmark_container">' +
			'<h1 id="bookmark_title"></h1>' +
			'<div id="bookmark_content">' +
				'<ul id="bookmarkList">' +
				'</ul>' +
			'</div>' +
		'</div>'
	);

	var mybookmark = fillRemoveBookmark();
	appendToBookmark(mybookmark);

	$('#bookmark_container').css({
		position: 'fixed',
		zIndex: '99999',
		padding: '0',
		margin: '0'
	});

	$('#bookmark_title').text(KomicResources.strings.bookmarksJS.popuptitle);
	$('#bookmark_content').addClass(type);

	$('#bookmark_container').css({
		minWidth: $('#bookmark_container').outerWidth(),
		maxWidth: $('#bookmark_container').outerWidth()
	});

	switch( type ) {
		case 'bookmark':
			$('#bookmarkList').after('<div id="bookmark_panel"><input type="button" id="bookmark_ok" /></div>');
			$('#bookmark_ok').attr('value', KomicResources.strings.shared.popupclose).click(function() {
				removeBookmark();
				hideMyBookmark();
				callback(true);
			});
			$('#bookmark_title').click(function() {
				removeBookmark();
				hideMyBookmark();
				callback(true);
			});
		break;
	}
	resizeBookmark();
}
function hideMyBookmark() {
	$('#bookmark_container').remove();
	$('#bookmark_overlay').remove();
}
function showMyBookmark() {
	$('#bookmark_overlay').remove();
	$('body').append('<div id="bookmark_overlay"></div>');
	$('#bookmark_overlay').css({
		position: 'absolute',
		zIndex: 99998,
		top: '0px',
		left: '0px',
		width: '100%',
		height: $(document).height(),
		background: '#FFF',
		opacity: '.01'
	});
}
function resizeBookmark() {
	var max_width = $(window).width() - 20;
		min_width = $(window).width() / 2,
		max_height = $(window).height() - 10,

	$('#bookmark_container').css({
		'min-width': min_width + 'px',
		'max-width': max_width + 'px',
		'max-height': max_height + 'px'
	});

	$('#bookmark_overlay').height( $(document).height() );

	var top = (($(window).height() / 2) - ($('#bookmark_container').outerHeight() / 2)) - 0,
		left = (($(window).width() / 2) - ($('#bookmark_container').outerWidth() / 2)) + 0;

	if (top < 0) top = 0;
	if (left < 0) left = 0;

	$('#bookmark_container').css({
		top: top + 'px',
		left: left + 'px'
	});
}
function appendToBookmark(data) {
	$('#bookmarkList li').empty();
	for (var i=0; i<data.length; i++) {
		if (data[i] != null) {
			var comic = data[i].comic;

			var $li = $('<li><b>' + comic + '</b></li>');
			$li.appendTo('#bookmarkList');
		}
	}
	$('#bookmarkList li').click(function(){
		if ($(this).data('selected') == true) {
			$(this).css({
				'text-decoration': 'none',
				'font-style': 'normal'
			}).data('selected', false);			
		} else if ($(this).data('selected') != true) {
			$(this).css({
				'text-decoration': 'underline',
				'font-style': 'italic'
			}).data('selected', true);
		}	
	});
}