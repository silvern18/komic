# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


source /mnt/us/extensions/komic/bin/resource/Scripts/resources.sh


TODELETE=$(sqlite3 /mnt/us/extensions/komic/bin/resource/LocalStorage/file__0.localstorage "select value from ItemTable where key = 'todelete'")
LENGTH=$(echo $TODELETE | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -l)
echo Number of comics to delete: $LENGTH

count=0
while [ "$count" -le $((LENGTH-1)) ]
do

	del=$(echo $TODELETE | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e "$count" | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e folder | sed 's/^.\(.*\).$/\1/')	
	echo Removing $del
	
	if [[ -n "$del" ]]
	then
		rm -rf "/mnt/us/documents/Komic/$del"
		deleted=1
		echo $(date "+%H:%M   %b %d, %Y")      "/mnt/us/documents/Komic/$del" >> /mnt/us/extensions/komic/bin/resource/Logs/deleted_log.txt
	fi
	count=$((count+1))
	togo=$(($LENGTH - $count))
	progressinside 39 "$count$processed$togo$stilltogo" sleep
done

if [[ -n "$deleted" ]]
then
	sh /mnt/us/extensions/komic/bin/resource/Scripts/refresh.sh
	lipc-set-prop com.silver18.komic001 communicationService deletionDone
	if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
	then
		/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Ding.wav
	fi
else
	lipc-set-prop com.silver18.komic001 communicationService deletionError
	if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
	then
		/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Error.wav
	fi
fi