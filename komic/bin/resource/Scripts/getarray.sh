# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


sleep 1

KOMICRESERVED=$(sqlite3 /mnt/us/extensions/komic/bin/resource/LocalStorage/file__0.localstorage "select value from ItemTable where key = 'komicReserved'")
CARTELLA=$(echo $KOMICRESERVED | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e komicData -e liveData -e lastread | sed 's/^.\(.*\).$/\1/')
KOMICPATH=/mnt/us/documents/Komic/
COMIC=${KOMICPATH}${CARTELLA}/
FILE="/mnt/us/extensions/komic/bin/resource/Temp/temp.txt"
ARRAY="/mnt/us/extensions/komic/bin/resource/Temp/pagename.js"
name1="previewlandscape.jpg"
name2="previewportrait.jpg"

rm -f "$ARRAY"
find "$COMIC/"  \( -name '*.jpg' -o -name '*.png' -o -name '*.PNG' -o -name '*.JPG' -o -name '*.bmp' -o -name '*.BMP' -o -name '*.gif' -o -name '*.GIF' \) | sed "s|$COMIC/||g" | sed -e s/"$name1"//g -e s/"$name2"//g | sed '/^$/d' | sort -n >> "$FILE"
firstname=$(sed -n 1p "$FILE" | awk -F\. '{print $1F}')

if [[ "$firstname" != "0" ]]
then

	awk -F "~~~" '{p=$0; gsub(/[^0-9]+/,FS); print substr(p,1,1)$0p}' "$FILE" > "$FILE".tmp
	rm -f "$FILE"	
	maxfields="$(awk -F "~~~" '{print NF}' "$FILE".tmp | sort -nr | head -1)"
	sort -k1,1 -k2,"$maxfields"n "$FILE".tmp | awk -F "~~~" '{print $NF}' > "$FILE"
	rm -f "$FILE".tmp

fi

echo "var arrayPageName = new Array (" >> "$ARRAY"
cat "$FILE" | sed 's/^/"/' | sed 's/$/",/' | sed '$s/.$//' | sed '$s/$/\n);/' >> "$ARRAY"
rm -f "$FILE"

lipc-set-prop com.silver18.komic001 communicationService arrayPageName