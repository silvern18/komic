# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


KOMICRESERVED=$(sqlite3 /mnt/us/extensions/komic/bin/resource/LocalStorage/file__0.localstorage "select value from ItemTable where key = 'komicReserved'")
DEBUGOPTION=$(echo $KOMICRESERVED | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e komicData -e options -e debugmessage | sed 's/^.\(.*\).$/\1/')


language=$(cat /var/local/system/locale | grep LANG | cut -c 6-10)
if [ "$language" = "it_IT" ]
then

	#start.sh
	export information="RACCOLGO INFORMAZIONI..."
	export coverviewdisabled="COVER VIEW DISABILITATA..."

	#preview.sh
	export creatingthumb="CREO LE MINIATURE..."

	#decompress.sh
	export filetoprocess=" FILE(S) DA PROCESSARE"
	export thisshould="QUESTO PROCESSO DOVREBBE IMPIEGARE" 
	export lessthanmin="MENO DI UN MINUTO"
	export morethanmin=" MINUTO/I" 
	export cbzbroken="FILE CBZ VUOTO O CORROTTO"
	export cbrbroken="FILE CBR VUOTO O CORROTTO"
	export processed=" FILE PROCESSATO/I, "
	export stilltogo=" ANCORA DA ELABORARE"
	
	#removeborders.sh
	export fileprocessed="FILE PROCESSATI: "
	
	#removeborderspreview.sh
    export workingon="LAVORO SUL FUMETTO: "
    export previewingthis="IMMAGINE SCELTA: "
    export toleranceis="TOLLERANZA SCELTA: "
    
    #eventlauncher.sh
    export launching="ESEGUO IL COMANDO: "

else

	#start.sh
	export information="GATHERING INFORMATION..."
	export coverviewdisabled="COVER VIEW DISABLED..."

	#preview.sh
	export creatingthumb="CREATING THUMBNAILS..."

	#decompress.sh
	export filetoprocess=" FILE(S) TO PROCESS"
	export thisshould="THIS SHOULD TAKE" 
	export lessthanmin="LESS THAN A MINUTE"
	export morethanmin=" MINUTE(S)"
	export cbzbroken="CBZ FILE EMPTY OR BROKEN" 
	export cbrbroken="CBR FILE EMPTY OR BROKEN" 
	export processed=" FILE PROCESSED, "
	export stilltogo=" STILL TO GO"

	#removeborders.sh
	export fileprocessed="FILE PROCESSED: "
	
	#removeborderspreview.sh
    export workingon="WORKING ON COMIC: "
    export previewingthis="PREVIEWING IMAGE: "
    export toleranceis="TOLERANCE IS: "
    
    #eventlauncher.sh
    export launching="RUNNING COMMAND: "

fi


progress()
{
	checkscreen
	local x=48
	local y=$1
	if [ "$3" = "sleep" ]
	then
		sleep 2
	fi
	if [ "$screenheight" = "600" ]
	then
		local y=$((y-10))
		local x=65
	fi
	eips 0 $y "                                                ";
	eips $((($x - $(expr length "$2")) / 2)) $y "$2"
}

progressinside()
{
	if [[ "$DEBUGOPTION" = "ON" ]]
	then
		local x=48
		local y=$1
		checkscreen
		if [ "$screenheight" = "600" ]
		then
			local y=$((y-10))
			local x=65
			eips 0 $y "                                                                  ";
		else
			eips 0 $y "                                                ";
		fi
#		eips 0 $y "                                                ";
		eips $((($x - $(expr length "$2")) / 2)) $y "$2"
		if [ "$3" = "sleep" ]
		then
			sleep 2
		fi
	fi
}

checkscreen()
{
	export screenheight="$(xrandr | grep '*' | cut -b 8-10)"
	if [ "$screenheight" = "" ]
	then
		KOMICRESERVED=$(sqlite3 /mnt/us/extensions/komic/bin/resource/LocalStorage/file__0.localstorage "select value from ItemTable where key = 'komicReserved'")
		currentorientation=$(echo $KOMICRESERVED | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e komicData -e liveData -e orientation | sed 's/^.\(.*\).$/\1/')
	
		if [ "$currentorientation" = "landscape" ]
		then
			export screenheight=600
		else
			export screenheight=800
		fi
	fi
}
