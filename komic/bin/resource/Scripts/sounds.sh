# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


source /mnt/us/extensions/komic/bin/resource/Scripts/resources.sh

if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
then
	touch /mnt/us/extensions/komic/bin/resource/Sounds/nosound
	lipc-set-prop com.silver18.komic001 communicationService soundsOFF
else
	rm -f /mnt/us/extensions/komic/bin/resource/Sounds/nosound
	lipc-set-prop com.silver18.komic001 communicationService soundsON
	/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Ding.wav
fi