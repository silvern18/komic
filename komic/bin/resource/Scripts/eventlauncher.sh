# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


lipc-wait-event -m com.lab126.system komicremovecomic,komicsounds,komicrefreshscreen,komicfreememory,komicscreenshot,komicgetarray,komicdecompress,komicgetfirstimage,komictitlebarON,komictitlebarOFF,komicremoveborder,komicremoveborderpreview,komicExitNow | while read event; do

	if [[ "$event" == "komicremovecomic" ]]; then
		sh /mnt/us/extensions/komic/bin/resource/Scripts/delete.sh
	elif [[ "$event" == "komicremoveborder" ]]; then
		sh /mnt/us/extensions/komic/bin/resource/Scripts/removeborders.sh
	elif [[ "$event" == "komicremoveborderpreview" ]]; then
		sh /mnt/us/extensions/komic/bin/resource/Scripts/removeborderspreview.sh
	elif [[ "$event" == "komicsounds" ]]; then
		sh /mnt/us/extensions/komic/bin/resource/Scripts/sounds.sh
	elif [[ "$event" == "komicrefreshscreen" ]]; then
		eips -f ''
	elif [[ "$event" == "komicfreememory" ]]; then
		sync; echo 3 >/proc/sys/vm/drop_caches
	elif [[ "$event" == "komicscreenshot" ]]; then
		sh /mnt/us/extensions/komic/bin/resource/Scripts/movescreenshots.sh
	elif [[ "$event" == "komicgetarray" ]]; then
		sh /mnt/us/extensions/komic/bin/resource/Scripts/getarray.sh
	elif [[ "$event" == "komicdecompress" ]]; then
		sh /mnt/us/extensions/komic/bin/resource/Scripts/decompress.sh	
	elif [[ "$event" == "komicgetfirstimage" ]]; then
		sh /mnt/us/extensions/komic/bin/resource/Scripts/getfirst.sh
	elif [[ "$event" == "komictitlebarON" ]]; then
		lipc-set-prop com.lab126.pillow interrogatePillow '{"pillowId": "default_status_bar", "function": "nativeBridge.showMe();"}'
	elif [[ "$event" == "komictitlebarOFF" ]]; then
		lipc-set-prop com.lab126.pillow interrogatePillow '{"pillowId": "default_status_bar", "function": "nativeBridge.hideMe();"}'
	elif [[ "$event" == "komicExitNow" ]]; then
		lipc-set-prop com.lab126.appmgrd stop com.silver18.komic001
	fi
	
done;
