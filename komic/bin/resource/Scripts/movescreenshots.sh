# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


source /mnt/us/extensions/komic/bin/resource/Scripts/resources.sh

cartella="/mnt/us/documents/Komic/@Screenshots@"
if [ -d "$cartella" ]
then
	echo "folder present"
else
	mkdir -p "$cartella"
fi
oldscreenshots="$(ls "$cartella"/*.jpg | wc -l)"
mv /mnt/us/screenshot*.png "$cartella" -f
find "$cartella/" -name '*.png' | sed "s|$cartella/||g" | sort -n > /mnt/us/extensions/komic/bin/resource/Temp/screenshots.txt
newscreenshots="$(wc -l < /mnt/us/extensions/komic/bin/resource/Temp/screenshots.txt)"
count=1
while read line
do
#while [ "$count" -le "$newscreenshots" ]
#do
	immagine=$(sed -n "$count"p /mnt/us/extensions/komic/bin/resource/Temp/screenshots.txt)
	vecchio=$cartella/$immagine
	nuovo=$cartella/$oldscreenshots.jpg
	mv -f "$vecchio" "$nuovo"
	echo $(date "+%H:%M   %b %d, %Y")      $vecchio >> /mnt/us/extensions/komic/bin/resource/Logs/screenshots_log.txt
	count=$((count+1))
	oldscreenshots=$((oldscreenshots+1))
	doneit=1
#done
done <"/mnt/us/extensions/komic/bin/resource/Temp/screenshots.txt"

rm -f /mnt/us/extensions/komic/bin/resource/Temp/screenshots.txt
if [[ -n "$doneit" ]]
then
	sh /mnt/us/extensions/komic/bin/resource/Scripts/refresh.sh
	lipc-set-prop com.silver18.komic001 communicationService screenshotsMoved
	if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
	then
		/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Ding.wav
	fi
	sleep 2
else
	lipc-set-prop com.silver18.komic001 communicationService screenshotsError
	if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
	then
		/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Error.wav
	fi
	sleep 2
fi