# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


source /mnt/us/extensions/komic/bin/resource/Scripts/resources.sh

KOMICPATH=/mnt/us/documents/Komic/
FILE="/mnt/us/extensions/komic/bin/resource/Temp/temp.txt"
IMAGEPATH="/mnt/us/extensions/komic/bin/resource/Temp/imagePath.js"
name1="previewlandscape.jpg"
name2="previewportrait.jpg"

rm -f "$FILE"
rm -f "$IMAGEPATH"
rm /mnt/us/extensions/komic/bin/resource/Temp/*.jpg *.png *.bmp *.gif *.JPG *.PNG *.BMP *.GIF

unset processed
unset KOMICBORDER
unset FOLDER
unset TOLERANCE

KOMICBORDER=$(sqlite3 /mnt/us/extensions/komic/bin/resource/LocalStorage/file__0.localstorage "select value from ItemTable where key = 'removeborders'")
FOLDER=$(echo $KOMICBORDER | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e folder | sed 's/^.\(.*\).$/\1/')
TOLERANCE=$(echo $KOMICBORDER | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e tolerance | sed 's/^.\(.*\).$/\1/')

COMIC=${KOMICPATH}${FOLDER}	
find "$COMIC/"  \( -name '*.jpg' -o -name '*.png' -o -name '*.PNG' -o -name '*.JPG'  -o -name '*.bmp' -o -name '*.BMP' -o -name '*.gif' -o -name '*.GIF' \) | sed "s|$COMIC/||g" | sed -e s/"$name1"//g -e s/"$name2"//g | sed '/^$/d' >> "$FILE"

LINES="$(wc -l < $FILE)"
RANDSEED=$(date '+%S%M%I')
IMAGE=$(cat $FILE | awk -v COUNT=$LINES -v SEED=$RANDSEED 'BEGIN { srand(SEED); \
i=int(rand()*COUNT) } FNR==i { print $0 }')

progressinside 39 "$workingon$COMIC" sleep
progressinside 39 "$previewingthis$IMAGE" sleep
progressinside 39 "$toleranceis$TOLERANCE" sleep

newName=/mnt/us/extensions/komic/bin/resource/Temp/$IMAGE
cp "$COMIC/$IMAGE" "$newName"

/mnt/us/extensions/komic/bin/resource/Utilities/convert -define jpeg:size=512 -fuzz "$TOLERANCE"% "$newName" -trim +repage "$newName"

echo 'var borderPreviewImagePath = "'${newName}'";' >> "$IMAGEPATH"

rm -f "$FILE"
processed=1


if [[ -n "$processed" ]]
then
	lipc-set-prop com.silver18.komic001 communicationService bordersPreviewDone
	if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
	then
		/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Ding.wav
	fi
else
	lipc-set-prop com.silver18.komic001 communicationService bordersPreviewError
	if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
	then
		/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Error.wav
	fi
fi
