# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


# This script checks every button pressed and, if you pressed the HOME button, it displays the status bar and kills itself
# This script also saves a bookmark if autosave is ON


while :;do
	set $(waitforkey); K=$1 D=$2
	[[ $D = 0 ]] && continue
	if [[ $K = 102 ]]
	then
        lipc-set-prop com.lab126.pillow interrogatePillow '{"pillowId":"default_status_bar","function":"nativeBridge.showMe();"}'		
		ps -ef | grep waitkey.sh | awk '{print $2}' | xargs kill -9
	fi
done