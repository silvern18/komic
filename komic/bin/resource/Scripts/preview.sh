# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


FILE="/mnt/us/extensions/komic/bin/resource/Temp/temp.txt"
source /mnt/us/extensions/komic/bin/resource/Scripts/resources.sh

find /mnt/us/documents/Komic/ -type f \( -name '*.jpg' -o -name '*.png' -o -name '*.PNG' -o -name '*.JPG'  -o -name '*.bmp' -o -name '*.BMP' -o -name '*.gif' -o -name '*.GIF' \) | sed 's#\(.*\)/.*#\1#' | sort -u > /mnt/us/extensions/komic/bin/resource/Temp/images.txt
find /mnt/us/documents/Komic/ -type f \( -name 'preview*.*' \) | sed 's#\(.*\)/.*#\1#' | sort -u > /mnt/us/extensions/komic/bin/resource/Temp/previews.txt
diff /mnt/us/extensions/komic/bin/resource/Temp/previews.txt /mnt/us/extensions/komic/bin/resource/Temp/images.txt | grep +/mnt/us/documents/Komic/ | sed -e 's/.//' > /mnt/us/extensions/komic/bin/resource/Temp/toupdate.txt
#numfiles="$(wc -l < /mnt/us/extensions/komic/bin/resource/Temp/toupdate.txt)"
count1=1
#while [ "$count1" -le "$numfiles" ]
#do
while read line
do
	folder=$(sed -n "$count1"p /mnt/us/extensions/komic/bin/resource/Temp/toupdate.txt)	
	firstname=$(ls "$folder" | sort -n | head -1 | awk -F\. '{print $1F}')
	
	if [[ "$firstname" != "0" ]]
	then
		
		ls "$folder" | awk -F "~~~" '{p=$0; gsub(/[^0-9]+/,FS); print substr(p,1,1)$0p}' > "$FILE".tmp
		maxfields="$(awk -F "~~~" '{print NF}' "$FILE".tmp | sort -nr | head -1)"
		firstimage=$(sort -k1,1 -k2,"$maxfields"n "$FILE".tmp | awk -F "~~~" '{print $NF}' | head -1)
		rm -f "$FILE".tmp
	
	else
		
		firstimage=$(ls "$folder" | sort -n | head -1)
	
	fi
	
	extension=$( echo $firstimage | awk -F\. '{print $NF}')
	vecchio=$folder/$firstimage
	
	if [[ "$extension" = "png" -o "$extension" = "PNG" -o "$extension" = "bmp" -o "$extension" = "BMP" -o "$extension" = "gif" -o "$extension" = "GIF" ]]
	then
		portrait=$folder/previewportrait.jpg
		landscape=$folder/previewlandscape.jpg
		/mnt/us/extensions/komic/bin/resource/Utilities/convert "$vecchio" -thumbnail 186 -quality 30 -flatten "$portrait"
		/mnt/us/extensions/komic/bin/resource/Utilities/convert "$vecchio" -thumbnail 252 -quality 30 -flatten "$landscape"
		echo $(date "+%H:%M   %b %d, %Y")      $folder >> /mnt/us/extensions/komic/bin/resource/Logs/preview_log.txt
	fi
	if [[ "$extension" = "jpg" -o "$extension" = "JPG" ]]
	then
		portrait=$folder/previewportrait.jpg
		landscape=$folder/previewlandscape.jpg
		/mnt/us/extensions/komic/bin/resource/Utilities/convert -define jpeg:size=512 "$vecchio" -thumbnail 186 -quality 30 "$portrait"
		/mnt/us/extensions/komic/bin/resource/Utilities/convert -define jpeg:size=512 "$vecchio" -thumbnail 252 -quality 30 "$landscape"
		echo $(date "+%H:%M   %b %d, %Y")      $folder >> /mnt/us/extensions/komic/bin/resource/Logs/preview_log.txt
	fi
	count1=$((count1+1))
#done
done <"/mnt/us/extensions/komic/bin/resource/Temp/toupdate.txt"

rm -f /mnt/us/extensions/komic/bin/resource/Temp/images.txt
rm -f /mnt/us/extensions/komic/bin/resource/Temp/previews.txt
rm -f /mnt/us/extensions/komic/bin/resource/Temp/toupdate.txt