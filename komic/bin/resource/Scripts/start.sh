# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1

source /mnt/us/extensions/komic/bin/resource/Scripts/resources.sh

sleep 1
progress 35 "$information" nosleep
find /mnt/us/documents/Komic/ -type f \( -name '*.jpg' -o -name '*.png' -o -name '*.PNG' -o -name '*.JPG'  -o -name '*.bmp' -o -name '*.BMP' -o -name '*.gif' -o -name '*.GIF' \) | sed 's#\(.*\)/.*#\1#' | sort -u | sed "s|/mnt/us/documents/Komic/||g" > /mnt/us/extensions/komic/bin/resource/Temp/folders.txt
sh /mnt/us/extensions/komic/bin/resource/Scripts/getfirst.sh
sh /mnt/us/extensions/komic/bin/resource/Scripts/volumes.sh
cat /mnt/us/extensions/komic/bin/resource/Temp/volumes.txt >> /mnt/us/extensions/komic/bin/resource/Temp/folders.txt
sort -o /mnt/us/extensions/komic/bin/resource/Temp/folders.txt /mnt/us/extensions/komic/bin/resource/Temp/folders.txt
sed -e 's_.*_"&_' /mnt/us/extensions/komic/bin/resource/Temp/folders.txt | sed 's/$/"/' > /mnt/us/extensions/komic/bin/resource/Temp/variables.js

count=1
righe="$(wc -l < /mnt/us/extensions/komic/bin/resource/Temp/variables.js)"
while read line
do
	sed -i "$count s_.*_var riga$count=&_" /mnt/us/extensions/komic/bin/resource/Temp/variables.js
	count=$((count+1)) 
done <"/mnt/us/extensions/komic/bin/resource/Temp/variables.js"

echo var numrighe=$righe >> /mnt/us/extensions/komic/bin/resource/Temp/variables.js
KOMICRESERVED=$(sqlite3 /mnt/us/extensions/komic/bin/resource/LocalStorage/file__0.localstorage "select value from ItemTable where key = 'komicReserved'")
preview=$(echo $KOMICRESERVED | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e komicData -e options -e preview | sed 's/^.\(.*\).$/\1/')
if [[ "$preview" = "ON" ]]
then
	progress 36 "$creatingthumb" nosleep
	sh /mnt/us/extensions/komic/bin/resource/Scripts/preview.sh
elif [[ "$preview" = "OFF" ]]
then
	progress 36 "$coverviewdisabled" nosleep
fi
rm -f /mnt/us/extensions/komic/bin/resource/Temp/folders.txt
rm -f /mnt/us/extensions/komic/bin/resource/Temp/volumes.txt

KOMICRESERVED=$(sqlite3 /mnt/us/extensions/komic/bin/resource/LocalStorage/file__0.localstorage "select value from ItemTable where key = 'komicReserved'")
pillowstate=$(echo $KOMICRESERVED | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e komicData -e options -e komictitlebar | sed 's/^.\(.*\).$/\1/')
if [[ "$pillowstate" = "OFF" ]]
then
	lipc-set-prop com.lab126.pillow interrogatePillow '{"pillowId": "default_status_bar", "function": "nativeBridge.hideMe();"}'
fi

soundstate=$(echo $KOMICRESERVED | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e komicData -e options -e komicsounds | sed 's/^.\(.*\).$/\1/')
if [[ "$soundstate" = "OFF" ]]
then
	touch /mnt/us/extensions/komic/bin/resource/Sounds/nosound
fi

killall mesquite
sync; echo 3 >/proc/sys/vm/drop_caches

lipc-set-prop com.lab126.appmgrd start app://com.silver18.komic001