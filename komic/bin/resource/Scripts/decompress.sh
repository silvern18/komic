# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


source /mnt/us/extensions/komic/bin/resource/Scripts/resources.sh
lipc-set-prop com.lab126.powerd preventScreenSaver 1

find /mnt/us/documents/Komic/ -type f \( -name '*.cbr' -o -name '*.cbz' \) | sort -u > /mnt/us/extensions/komic/bin/resource/Temp/compressed.txt
numfiles="$(wc -l < /mnt/us/extensions/komic/bin/resource/Temp/compressed.txt)"
if [[ "$numfiles" != "0" ]]
then
	progressinside 39 "$numfiles$filetoprocess" nosleep
fi

#sed -i "s|/mnt/us/documents/Komic/||g" /mnt/us/extensions/komic/bin/resource/Temp/compressed.txt
count1=1

while [ "$count1" -le "$numfiles" ]
do
	progressinside 39 "$processing" sleep
	file=$(sed -n "$count1"p /mnt/us/extensions/komic/bin/resource/Temp/compressed.txt)	
	if [[ -n "$file" ]]
	then
		error=
		nome=$(echo ${file} | sed 's/.\{4\}$//')
		extension=$(echo ${file} | awk -F\. '{print $NF}')
		vecchio_nome=$file
		cartella=$nome		
		mkdir "$cartella"	
		if [[ "$extension" = "cbz" ]]
		then
			nuovo_nome=$nome.zip
			mv "$vecchio_nome" "$nuovo_nome"
			unzip "$nuovo_nome" -d "$cartella"
			rm -f "$nuovo_nome"
			if [[ -z $(ls -A "$cartella") ]]
			then
				error=1
				rm -rf "$cartella"
				progressinside 39 "$cbzbroken" sleep
				if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
				then
					/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Error.wav
				fi
				echo $(date "+%H:%M   %b %d, %Y")      "$vecchio_nome" >> /mnt/us/extensions/komic/bin/resource/Logs/decompression_errors_log.txt
			fi
		fi
		if [[ "$extension" = "cbr" ]]
		then
			nuovo_nome=$nome.rar
			mv "$vecchio_nome" "$nuovo_nome"
			mv "$nuovo_nome" "$cartella"
			cd "$cartella"
			thisone=$(ls)
			/mnt/us/extensions/komic/bin/resource/Utilities/unrar e "$thisone"			
			rm -f "$thisone"
			if [[ -z $(ls -A "$cartella") ]]
			then
				error=1
				rm -rf "$cartella"
				progressinside 39 "$cbrbroken" sleep
				if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
				then
					/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Error.wav
				fi
				echo $(date "+%H:%M   %b %d, %Y")      "$vecchio_nome" >> /mnt/us/extensions/komic/bin/resource/Logs/decompression_errors_log.txt
			fi
			cd /
		fi
		if [[ -z "$error" ]]
		then
			echo $(date "+%H:%M   %b %d, %Y")      "$vecchio_nome" >> /mnt/us/extensions/komic/bin/resource/Logs/decompressed_log.txt
		fi
		togo=$(($numfiles - $count1))
		progressinside 39 "$count1$processed$togo$stilltogo" sleep
		doneit=1
	fi
	count1=$((count1+1))
done


rm -f /mnt/us/extensions/komic/bin/resource/Temp/compressed.txt
sleep 1
if [[ -n "$doneit" ]]
then
	sh /mnt/us/extensions/komic/bin/resource/Scripts/refresh.sh
	lipc-set-prop com.silver18.komic001 communicationService decompressDone
	if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
	then
		/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Ding.wav
	fi
else
	lipc-set-prop com.silver18.komic001 communicationService decompressError
	if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
	then
		/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Error.wav
	fi
fi
lipc-set-prop com.lab126.powerd preventScreenSaver 0