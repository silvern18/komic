# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


KOMICPATH=/mnt/us/documents/Komic/
FOLDERS=/mnt/us/extensions/komic/bin/resource/Temp/folders.txt
FILE="/mnt/us/extensions/komic/bin/resource/Temp/temp.txt"
LIST="/mnt/us/extensions/komic/bin/resource/Temp/list.txt"
IMAGE="/mnt/us/extensions/komic/bin/resource/Temp/firstimages.js"
name1="previewlandscape.jpg"
name2="previewportrait.jpg"

rm -f "$IMAGE"
cp "$FOLDERS" "$FILE"
#righe="$(wc -l < ${FILE})"
count=1
while read line
do
#while [ "$count" -le "$righe" ]
#do

	folder=$(sed -n "$count"p ${FILE})
	COMIC=${KOMICPATH}${folder}/
	firstname=$(find "$COMIC/"  \( -name '*.jpg' -o -name '*.png' -o -name '*.PNG' -o -name '*.JPG'  -o -name '*.bmp' -o -name '*.BMP' -o -name '*.gif' -o -name '*.GIF' \) | sed "s|$COMIC/||g" | sed -e s/"$name1"//g -e s/"$name2"//g | sed '/^$/d' | sort -n | head -1)
	onlyname=$(echo $firstname | awk -F\. '{print $1F}')

	if [[ "$onlyname" != "0" ]]
	then

		find "$COMIC/"  \( -name '*.jpg' -o -name '*.png' -o -name '*.PNG' -o -name '*.JPG'  -o -name '*.bmp' -o -name '*.BMP' -o -name '*.gif' -o -name '*.GIF' \) | sed "s|$COMIC/||g" | sed -e s/"$name1"//g -e s/"$name2"//g | sed '/^$/d' | sort -n >> "$LIST"

		awk -F "~~~" '{p=$0; gsub(/[^0-9]+/,FS); print substr(p,1,1)$0p}' "$LIST" > "$LIST".tmp
		rm -f "$LIST"
		maxfields="$(awk -F "~~~" '{print NF}' "$LIST".tmp | sort -nr | head -1)"
		firstname=$(sort -k1,1 -k2,"$maxfields"n "$LIST".tmp | awk -F "~~~" '{print $NF}' | head -1)
		rm -f "$LIST".tmp

	fi
	echo $firstname >> "$IMAGE".tmp
	count=$((count+1))
#done
done <"$FILE"
echo "var arrayFirstImages = new Array (" >> "$IMAGE"
cat "$IMAGE".tmp | sed 's/^/"/' | sed 's/$/",/' | sed '$s/.$//' | sed '$s/$/\n);/' >> "$IMAGE"

rm -f "$IMAGE".tmp
rm -f "$FILE"