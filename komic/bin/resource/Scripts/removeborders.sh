# Written by Silver18
# more info at: http://www.mobileread.com/forums/showpost.php?p=2121570&postcount=1


source /mnt/us/extensions/komic/bin/resource/Scripts/resources.sh

KOMICPATH=/mnt/us/documents/Komic/
FILE="/mnt/us/extensions/komic/bin/resource/Temp/temp.txt"
name1="previewlandscape.jpg"
name2="previewportrait.jpg"

rm -f "$FILE"

unset processed
unset KOMICBORDER
unset FOLDER
unset TOLERANCE

lipc-set-prop com.lab126.powerd preventScreenSaver 1

KOMICBORDER=$(sqlite3 /mnt/us/extensions/komic/bin/resource/LocalStorage/file__0.localstorage "select value from ItemTable where key = 'removeborders'")
FOLDER=$(echo $KOMICBORDER | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e folder | sed 's/^.\(.*\).$/\1/')
TOLERANCE=$(echo $KOMICBORDER | /mnt/us/extensions/komic/bin/resource/Utilities/jshon -e tolerance | sed 's/^.\(.*\).$/\1/')

COMIC=${KOMICPATH}${FOLDER}

echo Working on $COMIC
echo Tolerance is $TOLERANCE

echo $(date "+%H:%M   %b %d, %Y")      "$COMIC" >> /mnt/us/extensions/komic/bin/resource/Logs/debordered_log.txt

find "$COMIC/"  \( -name '*.jpg' -o -name '*.png' -o -name '*.PNG' -o -name '*.JPG' -o -name '*.bmp' -o -name '*.BMP' -o -name '*.gif' -o -name '*.GIF' \) | sed "s|$COMIC/||g" | sed -e s/"$name1"//g -e s/"$name2"//g | sed '/^$/d' | sort -n >> "$FILE"
count=0
while read line
do
	echo Now trimming: "$line"

	/mnt/us/extensions/komic/bin/resource/Utilities/convert -define jpeg:size=512 -fuzz "$TOLERANCE"% "$COMIC/$line" -trim +repage "$COMIC/$line"
	
	count=$((count+1))
	progressinside 39 "$fileprocessed$count" nosleep
	
	processed=1
	
done <"$FILE"

if [[ -n "$processed" ]]
then
	lipc-set-prop com.silver18.komic001 communicationService bordersDone
	if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
	then
		/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Ding.wav
	fi
else
	lipc-set-prop com.silver18.komic001 communicationService bordersError
	if ! [[ -e /mnt/us/extensions/komic/bin/resource/Sounds/nosound ]]
	then
		/usr/bin/aplay /mnt/us/extensions/komic/bin/resource/Sounds/Error.wav
	fi
fi

lipc-set-prop com.lab126.powerd preventScreenSaver 0